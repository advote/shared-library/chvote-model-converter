# CHVote: Model converter
[![pipeline status](https://gitlab.com/chvote2/shared-libraries/chvote-model-converter/badges/master/pipeline.svg)](https://gitlab.com/chvote2/shared-libraries/chvote-model-converter/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-model-converter&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-model-converter)

A library for the conversion of eCH files into protocol model objects.

# Usage

## Add to your project

Maven:
```xml
<!-- API Module -->
<dependency>
    <groupId>ch.ge.ve.interfaces</groupId>
    <artifactId>chvote-model-converter-api</artifactId>
    <version>1.0.19</version>
</dependency>

<!-- Implementation module -->
<dependency>
    <groupId>ch.ge.ve.interfaces</groupId>
    <artifactId>chvote-model-converter-impl</artifactId>
    <version>1.0.19</version>
</dependency>
```

# Building

## Pre-requisites

* JDK 8
* Maven

## Build steps

```bash
mvn clean install
```

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software released under the [Affero General Public License 3.0](https://gitlab.com/chvote2/shared-libraries/chvote-model-converter/blob/master/LICENSE) 
license.
