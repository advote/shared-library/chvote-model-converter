/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.model;

import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 * Model class representing a voter.
 */
public final class VoterDetails {

  private final int                     id;
  private final String                  registerPersonId;
  private final PartialLocalDate        dateOfBirth;
  private final CountingCircle          countingCircle;
  private final String                  printingAuthorityName;
  private final List<DomainOfInfluence> allowedDomainsOfInfluence;

  public VoterDetails(int id, String registerPersonId, PartialLocalDate dateOfBirth,
                      CountingCircle countingCircle, String printingAuthorityName,
                      List<DomainOfInfluence> allowedDomainsOfInfluence) {
    this.id = id;
    this.registerPersonId = Preconditions.checkNotNull(registerPersonId, "'registerPersonId' must not be null");
    this.dateOfBirth = Preconditions.checkNotNull(dateOfBirth, "'dateOfBirth' must not be null");
    this.countingCircle = Preconditions.checkNotNull(countingCircle, "'countingCircle' must not be null");
    this.printingAuthorityName = Preconditions.checkNotNull(printingAuthorityName,
                                                            "'printingAuthorityName' must not be null");
    this.allowedDomainsOfInfluence = ImmutableList.copyOf(allowedDomainsOfInfluence);
  }

  /**
   * Returns the voter's identifier. This id can be used as the voter index in the protocol representation of this
   * voter.
   *
   * @return the voter's identifier.
   */
  public int getId() {
    return id;
  }

  /**
   * Returns the identifier of this voter as defined in the source (ech-0045) register.
   *
   * @return the identifier of this voter in the source register.
   */
  public String getRegisterPersonId() {
    return registerPersonId;
  }

  /**
   * Returns the date of birth of this voter.
   *
   * @return the voter's date of birth.
   */
  public PartialLocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  /**
   * Returns the voter's counting circle.
   *
   * @return the voter's counting circle.
   */
  public CountingCircle getCountingCircle() {
    return countingCircle;
  }

  /**
   * Returns the name of the printing authority in charge of printing this voter's voting card.
   *
   * @return the name of the printing authority in charge of printing this voter's voting card.
   */
  public String getPrintingAuthorityName() {
    return printingAuthorityName;
  }

  /**
   * Returns the domain of influences this voter is allowed to vote.
   *
   * @return the domain of influences this voter is allowed to vote.
   */
  public List<DomainOfInfluence> getAllowedDomainsOfInfluence() {
    return allowedDomainsOfInfluence;
  }
}
