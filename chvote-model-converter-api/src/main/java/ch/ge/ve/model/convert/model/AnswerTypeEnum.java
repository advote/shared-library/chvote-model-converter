/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.model;

import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.List;

/**
 * An enum with all the possible answer types of a votation (as they appear in the eCH-0155 definition).
 */
@SuppressWarnings("squid:S1192") // It will be redundant to extract the constant, it's already in an static context.
public enum AnswerTypeEnum {
  /**
   * eCH-0155 index: 1. The answer type for questions whose options are: YES or NO.
   */
  YES_NO("YES", "NO"),
  /**
   * eCH-0155 index: 2. The answer type for questions whose options are: YES, NO or BLANK.
   */
  YES_NO_BLANK("YES", "NO", "BLANK"),
  /**
   * eCH-0155 index: 3. The answer type for questions whose options are: MARKED or UNMARKED.
   */
  MARKED_UNMARKED("MARKED", "UNMARKED"),
  /**
   * eCH-0155 index: 4. The answer type for questions whose options are: INITIATIVE, COUNTER_PROJECT or BLANK.
   */
  INITIATIVE_COUNTER_PROJECT_BLANK("INITIATIVE", "COUNTER_PROJECT", "BLANK"),
  /**
   * eCH-0155 index: 5. The answer type for questions whose options are: INITIATIVE, COUNTER_PROPOSAL or BLANK.
   */
  INITIATIVE_COUNTER_PROPOSITION_BLANK("INITIATIVE", "COUNTER_PROPOSAL", "BLANK"),
  /**
   * eCH-0155 index: 6. The answer type for questions whose options are: OBJECT, POPULAR_PROPOSITION or BLANK.
   */
  OBJECT_POPULAR_PROPOSITION_BLANK("OBJECT", "POPULAR_PROPOSITION", "BLANK"),
  /**
   * eCH-0155 index: 7. The answer type for questions whose options are: OBJECT, SUBSIDIARY_PROPOSITION or BLANK.
   */
  OBJECT_SUBSIDIARY_PROPOSITION_BLANK("OBJECT", "SUBSIDIARY_PROPOSITION", "BLANK");

  private final String[] options;

  AnswerTypeEnum(String... options) {
    this.options = options;
  }

  /**
   * Get the list of options of this answer type.
   *
   * @return the list of options of this answer type.
   */
  public List<String> getOptions() {
    return Arrays.asList(options);
  }

  /**
   * Retrieve the enum value by its corresponding eCH-0155 index value.
   *
   * @param index the eCH-0155 answer type index.
   *
   * @return the enum value for the given index.
   */
  public static AnswerTypeEnum fromInt(int index) {
    Preconditions.checkElementIndex(index, values().length);
    return values()[index - 1];
  }
}
