/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.api;

import ch.ge.ve.model.convert.model.VoterDetails;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This interface defines the contract for mapping the voters defined in eCH-0045 XML files into a model usable by the
 * protocol.
 */
public interface VoterConverter {
  /**
   * Convert the contents of eCH-0045 input streams into a stream of voters as needed for the
   * {@link ch.ge.ve.protocol.model.ElectionSet} used in the CHvote protocol.
   * <p>
   * Warning: all the voters must be provided together, so that the identifiers can be correctly
   * generated from 0 to the number of voters minus 1, without any duplicate key.
   *
   * @param printingAuthorityByMunicipality Printing authorities are uniquely defined for a municipality.
   * @param printingAuthoritiesByCanton     When no municipality is provided for a voter, use a canton-based mapping
   *                                        as fallback
   * @param voterDeliveriesInputStreams     the input streams containing eCH-0045 xml data
   *
   * @return a stream of voters corresponding to those defined in the input streams
   *
   * @throws MissingExtensionException if the voter does not contain the required counting circle definition.
   */
  Stream<VoterDetails> convertToVoterList(Map<Integer, String> printingAuthorityByMunicipality,
                                          Map<String, String> printingAuthoritiesByCanton,
                                          InputStream... voterDeliveriesInputStreams);

  /**
   * Convert the contents of the eCH-0045 input streams into a single eCH-0045 output streams for each printer
   *
   * @param voterDeliveriesByPrinters       The output streams receiving eCH-0045 xml data
   * @param printingAuthorityByMunicipality Printing authorities are uniquely defined for a municipality.
   * @param printingAuthorityByCanton       When no municipality is provided for a voter, use a canton-based mapping
   *                                        as fallback
   * @param numberOfVotersByPrinter         The number of voters by printing authority,
   *                                        see {@link StatsCounter}.
   * @param voterDeliveries                 the input streams containing eCH-0045 xml data.
   *
   * @throws PrinterMappingMismatchException if a printer is missing in the {@code voterDeliversByPrinters} map
   */
  void remapByPrinter(Map<String, OutputStream> voterDeliveriesByPrinters,
                      Map<Integer, String> printingAuthorityByMunicipality,
                      Map<String, String> printingAuthorityByCanton,
                      Map<String, Long> numberOfVotersByPrinter,
                      InputStream[] voterDeliveries);

}
