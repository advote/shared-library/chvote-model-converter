/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.api;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A simple component that counts the number of items having similar attributes.
 * <p>
 * A function is given to define how to extract the grouping key. The counter can then be queried to retrieve the number
 * of items matching a key.
 * </p>
 * <p>
 * Usage :
 * <pre>
 *     // Can be used as a step in a Stream :
 *     StatsCounter&lt;Voter, String&gt; counter = new StatsCounter&lt;&gt;(Voter::getPrintingAuthorityName);
 *     someCollection.stream()
 *         .map(toVoterFunction)
 *         .peek(counter::append)
 *         .forEach(System.out::println);
 *
 *     // .. or you can pass a whole collection through the counter :
 *     counter.applyTo(someCollection);
 *
 *     long voters_for_printer_1 = counter.getCountFor("printer 1");
 *     long voters_for_printer_2 = counter.getCountFor("printer 2");
 *   </pre>
 *
 * @param <E> the type of supported items
 * @param <K> the type of the key
 */
public class StatsCounter<E, K> {

  private final Function<E, K>     keyExtractor;
  private final Map<K, AtomicLong> numbers = new ConcurrentHashMap<>();

  /**
   * Defines a {@code StatsCounter} for a given grouping function
   *
   * @param keyExtractor function to get the key the item should be accounted for
   */
  public StatsCounter(Function<E, K> keyExtractor) {
    this.keyExtractor = keyExtractor;
  }

  /**
   * Count an item under the appropriate key.
   * <p>
   * The key will be derived from the item using the function defined at construction time.
   * </p>
   *
   * @param element the item to be counted
   */
  public void append(E element) {
    K key = keyExtractor.apply(element);
    numbers.putIfAbsent(key, new AtomicLong());

    numbers.get(key).incrementAndGet();
  }

  /**
   * Get the number of counted items for a given key value.
   *
   * @param key the key value, matching in regards to the {@code equals} method the one derived from an item
   *
   * @return the number of matching items counted, 0 if none matched such a key
   */
  public long getCountFor(K key) {
    return numbers.getOrDefault(key, new AtomicLong()).get();
  }

  /**
   * @return the total number of counted items
   */
  public long getTotal() {
    return numbers.values().stream().mapToLong(AtomicLong::get).sum();
  }

  /**
   * Take all items included in a collection and count them.
   *
   * @param collection the collection to count the items from
   */
  public void applyTo(Collection<? extends E> collection) {
    collection.forEach(this::append);
  }

  /**
   * Extract the current statistics as a Map.
   *
   * @return the mapping of frequency by key value, in a new and unconnected Map instance
   */
  public Map<K, Long> asMap() {
    return numbers.entrySet().stream()
                  .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().get()));
  }

  /**
   * Extract the current statistics as a Map, with the ability of remapping the keys.
   * <p>
   * The items are not counted anew : this function actually does a remapping of the keys using a given remapping
   * function, and sums the frequencies of keys that map to a similar value.
   * <br>
   * It is intended to regroup frequencies under a less selective key.
   * </p>
   *
   * @param toAggregatedKey remapping function that derives the new keys from the current ones
   * @param <U>             type of the new keys
   *
   * @return the mapping of frequency by key value, in a new and unconnected Map instance
   */
  public <U> Map<U, Long> asAggregatedMap(Function<K, U> toAggregatedKey) {
    return stream().collect(Collectors.toMap(
        e -> toAggregatedKey.apply(e.getKey()),
        Map.Entry::getValue,
        Long::sum
    ));
  }

  /**
   * Stream the current statistics, equivalent to: <code>statsCounter.asMap().entrySet().stream()</code> but the values
   * are immutable.
   *
   * @return a stream of the current statistics.
   */
  public Stream<Map.Entry<K, Long>> stream() {
    return numbers.entrySet().stream()
                  .map(e -> new StatsEntry<>(e.getKey(), e.getValue().get()));
  }


  // private, read-only representation of a Map.Entry
  private static class StatsEntry<K> implements Map.Entry<K, Long> {
    private final K    key;
    private final long value;

    StatsEntry(K key, long value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public K getKey() {
      return key;
    }

    @Override
    public Long getValue() {
      return value;
    }

    @Override
    public Long setValue(Long value) {
      throw new UnsupportedOperationException("This entry is read-only");
    }
  }

}
