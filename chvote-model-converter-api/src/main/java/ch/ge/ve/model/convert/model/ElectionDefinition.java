/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.model;

import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.Election;
import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 * This model class holds together the definition of an election and its candidates.
 * <p>
 * It is never used as such in the CHvote protocol, but is needed as an intermediate result when converting eCH-0157 and
 * eCH-0159 files.
 * </p>
 */
public final class ElectionDefinition {
  private final Election        election;
  private final List<Candidate> candidates;

  public ElectionDefinition(Election election, List<Candidate> candidates) {
    this.election = election;
    this.candidates = ImmutableList.copyOf(candidates);
  }

  public Election getElection() {
    return election;
  }

  public List<Candidate> getCandidates() {
    return candidates;
  }
}
