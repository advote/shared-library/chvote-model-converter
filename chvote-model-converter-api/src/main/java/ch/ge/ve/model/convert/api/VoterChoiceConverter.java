/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.api;

import ch.ge.ve.model.convert.model.VoterChoice;
import ch.ge.ve.protocol.model.ElectionSet;
import ch.ge.ve.protocol.model.PrintingAuthority;
import java.io.InputStream;
import java.util.List;

/**
 * This interface defines the contract for the conversion of an eCH-0159/eCH-0157 xml data file into a list of voter
 * choices.
 */
public interface VoterChoiceConverter {
  /**
   * Convert the contents of the given eCH-0159/eCH-0157 input streams into a list of voter choices in the order
   * defined by the given election set.
   *
   * @param electionSet     the election set
   * @param deliveryStreams the input streams containing all eCH-0159 and eCH-0157 xml data
   *
   * @return a list of voter choices corresponding to the combination of the election set and eCH-0159/eCH-0157 streams
   */
  List<VoterChoice> convertToVoterChoiceList(ElectionSet<? extends PrintingAuthority> electionSet,
                                             InputStream... deliveryStreams);
}
