/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.model;

import com.google.common.base.Preconditions;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.util.Optional;

/**
 * Represents a potentially partial local date.
 */
public final class PartialLocalDate {

  /**
   * Supported types of partial local dates.
   */
  public enum Type {
    YEAR,
    YEAR_MONTH,
    YEAR_MONTH_DAY;
  }

  private final Type    type;
  private final Year    year;
  private final Month   monthOfYear;
  private final Integer dayOfMonth;

  public PartialLocalDate(LocalDate date) {
    this(Type.YEAR_MONTH_DAY, Year.of(date.getYear()), date.getMonth(), date.getDayOfMonth());
  }

  public PartialLocalDate(YearMonth yearMonth) {
    this(Type.YEAR_MONTH, Year.of(yearMonth.getYear()), yearMonth.getMonth(), null);
  }

  public PartialLocalDate(Year year) {
    this(Type.YEAR, Preconditions.checkNotNull(year), null, null);
  }

  private PartialLocalDate(Type type, Year year, Month monthOfYear, Integer dayOfMonth) {
    this.type = type;
    this.year = year;
    this.monthOfYear = monthOfYear;
    this.dayOfMonth = dayOfMonth;
  }

  public Type getType() {
    return type;
  }

  public Year getYear() {
    return year;
  }

  public Optional<Month> getMonthOfYear() {
    return Optional.ofNullable(monthOfYear);
  }

  public Optional<Integer> getDayOfMonth() {
    return Optional.ofNullable(dayOfMonth);
  }
}
