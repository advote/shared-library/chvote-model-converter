/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.api;

import ch.ge.ve.model.convert.model.ElectionDefinition;
import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.List;

/**
 * This builder class helps constructing an ElectionSet as it is needed by the CHvote protocol.
 */
public class ElectionSetBuilder {
  private final List<Candidate>                      candidates          = new ArrayList<>();
  private final List<Election>                       elections           = new ArrayList<>();
  private final List<PrintingAuthorityWithPublicKey> printingAuthorities = new ArrayList<>();
  private       long                                 voterCount          = -1L;
  private       int                                  countingCircleCount = -1;

  public ElectionSetBuilder addElectionDefinitions(List<ElectionDefinition> electionDefinitions) {
    boolean areElectionDefinitionsValid = electionDefinitions.stream().allMatch(
        ed -> ed.getElection().getNumberOfCandidates() == ed.getCandidates().size()
              && ed.getElection().getNumberOfCandidates() > ed.getElection().getNumberOfSelections()
    );
    Preconditions.checkArgument(areElectionDefinitionsValid, "The election definitions must be valid");
    for (ElectionDefinition electionDefinition : electionDefinitions) {
      elections.add(electionDefinition.getElection());
      candidates.addAll(electionDefinition.getCandidates());
    }
    return this;
  }

  public ElectionSetBuilder addPrintingAuthorities(List<PrintingAuthorityWithPublicKey> printingAuthorities) {
    this.printingAuthorities.addAll(printingAuthorities);
    return this;
  }

  public ElectionSetBuilder setVoterCount(long voterCount) {
    Preconditions.checkArgument(voterCount > 0L, "There must be at least one voter");
    this.voterCount = voterCount;
    return this;
  }

  public ElectionSetBuilder setCountingCircleCount(int countingCircleCount) {
    Preconditions.checkArgument(countingCircleCount > 0, "There must be at least one counting circle");
    this.countingCircleCount = countingCircleCount;
    return this;
  }

  /**
   * Finalize the construction of the {@link ch.ge.ve.protocol.model.ElectionSet}
   *
   * @return an election set reflecting the values added to this point
   */
  public final ElectionSetWithPublicKey build() {
    Preconditions.checkState(!elections.isEmpty(), "The election definitions need to have been set first");
    Preconditions.checkState(!printingAuthorities.isEmpty(), "The printing authorities must have been defined first");
    Preconditions.checkState(voterCount > 0L, "The voter count must have been set first");
    Preconditions.checkState(countingCircleCount > 0, "The counting circle count must have been set first");
    return new ElectionSetWithPublicKey(elections, candidates, printingAuthorities, voterCount, countingCircleCount);
  }
}
