/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.api;

/**
 * Thrown when a reference file does not contain the required election identifier.
 */
public class MissingElectionIdentifierException extends RuntimeException {

  private static final String DEFAULTMESSAGE_FORMAT = "No election found for election with identifier [%s]";

  /**
   * Construct a new missing election identifier exception with the default message formatted with the given election
   * identifier.
   *
   * @param electionIdentifier the missing election identifier.
   *
   * @see RuntimeException#RuntimeException(String)
   */
  public MissingElectionIdentifierException(String electionIdentifier) {
    super(String.format(DEFAULTMESSAGE_FORMAT, electionIdentifier));
  }

}
