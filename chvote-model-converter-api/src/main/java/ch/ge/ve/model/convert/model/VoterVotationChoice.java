/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.model;

import ch.ge.ve.protocol.model.Election;

/**
 * A specialised {@link VoterChoice} that contains additional metadata for a votation choice.
 */
public class VoterVotationChoice extends VoterChoice {
  private final String         ballotId;
  private final AnswerTypeEnum answerType;
  private final int            answerRank;
  private final boolean        isStandardBallot;
  private final boolean        isVariantBallot;

  public VoterVotationChoice(Election election,
                             String voteId,
                             String candidateId,
                             String ballotId,
                             AnswerTypeEnum answerType,
                             boolean isStandardBallot,
                             boolean isVariantBallot) {
    super(election, voteId, candidateId);

    this.ballotId = ballotId;
    this.answerType = answerType;
    this.answerRank = answerType.getOptions().indexOf(candidateId);
    this.isStandardBallot = isStandardBallot;
    this.isVariantBallot = isVariantBallot;
  }

  public String getBallotId() {
    return ballotId;
  }

  public AnswerTypeEnum getAnswerType() {
    return answerType;
  }

  public int getAnswerRank() {
    return answerRank;
  }

  public boolean isStandardBallot() {
    return isStandardBallot;
  }

  public boolean isVariantBallot() {
    return isVariantBallot;
  }
}
