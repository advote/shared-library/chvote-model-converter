/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.api;

import ch.ge.ve.model.convert.model.ElectionConfiguration;
import ch.ge.ve.model.convert.model.ElectionDefinition;
import java.io.InputStream;
import java.util.List;

/**
 * This interface defines the contract for the conversion of an eCH-0157 xml data file into a list of election
 * definitions.
 * <p>
 * <p>Election is used here, as in the CHvote system specification, in the broadest of senses.</p>
 */
public interface ElectionConverter {
  /**
   * Convert the contents of eCH-0157 input streams into a list of election definitions that can be used for creating
   * an {@link ch.ge.ve.protocol.model.ElectionSet} used in the CHvote protocol
   *
   * @param electionDeliveryStreams the input streams containing eCH-0157 xml data
   *
   * @return a list of election definitions corresponding to the combined input streams
   */
  List<ElectionDefinition> convertToElectionDefinitionList(InputStream... electionDeliveryStreams);

  /**
   * Convert the contents of eCH-0157 input streams into a list of election definitions that can be used for creating
   * an {@link ch.ge.ve.protocol.model.ElectionSet} used in the CHvote protocol
   *
   * @param electionConfigurations  additional configuration parameters to customize the election.
   * @param electionDeliveryStreams the input streams containing eCH-0157 xml data
   *
   * @return a list of election definitions corresponding to the combined input streams
   */
  List<ElectionDefinition> convertToElectionDefinitionList(List<ElectionConfiguration> electionConfigurations,
                                                           InputStream... electionDeliveryStreams);
}

