/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.model;

import java.io.InputStream;
import java.util.List;

/**
 * An object that provides additional configuration parameters for an election.
 *
 * @see ch.ge.ve.model.convert.api.ElectionConverter#convertToElectionDefinitionList(List, InputStream...)
 */
public class ElectionConfiguration {
  private final String  ballotIdentifier;
  private final boolean hasVoidElectoralRollWithNoCandidates;

  /**
   * Create a new election configuration.
   *
   * @param ballotIdentifier                     identifies the targeted ballot of this configuration.
   * @param hasVoidElectoralRollWithNoCandidates whether it should be possible to choose an empty list with no
   *                                             candidates.
   */
  public ElectionConfiguration(String ballotIdentifier, boolean hasVoidElectoralRollWithNoCandidates) {
    this.ballotIdentifier = ballotIdentifier;
    this.hasVoidElectoralRollWithNoCandidates = hasVoidElectoralRollWithNoCandidates;
  }

  public String getBallotIdentifier() {
    return ballotIdentifier;
  }

  public boolean isHasVoidElectoralRollWithNoCandidates() {
    return hasVoidElectoralRollWithNoCandidates;
  }
}
