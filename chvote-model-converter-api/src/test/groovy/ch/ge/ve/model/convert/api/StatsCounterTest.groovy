/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.api

import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.protocol.model.Voter
import java.util.concurrent.atomic.AtomicInteger
import spock.lang.Specification

class StatsCounterTest extends Specification {

  def idGenerator = new AtomicInteger()

  /** a function that extracts the "printingAuthorityName" */
  def byPrintingAuthority = { Voter voter ->
    return voter.printingAuthorityName
  }

  /** a function that extracts the "printingAuthorityName" and the id of the "countingCircle" */
  def byPrintingAuthorityAndCountingCircle = { Voter voter ->
    return [voter.printingAuthorityName, voter.countingCircle?.id]
  }

  def newVoterForPrinter(String printerAuthorityName) {
    return new Voter(idGenerator.incrementAndGet(), "", null, printerAuthorityName)
  }

  def newVoterForPrinterAndCountingCircle(String printerAuthorityName, int countingCircleId) {
    return new Voter(idGenerator.incrementAndGet(), "", new CountingCircle(countingCircleId, "", ""), printerAuthorityName)
  }

  def "append can count from single key"() {
    given:
    def counter = new StatsCounter(byPrintingAuthority)

    when:
    counter.append(newVoterForPrinter("printer1"))
    counter.append(newVoterForPrinter("printer2"))
    counter.append(newVoterForPrinter("printer1"))

    then:
    counter.numbers.size() == 2
    counter.getCountFor("printer1") == 2
    counter.getCountFor("printer2") == 1
  }

  def "append can count from multi-values key"() {
    given:
    def counter = new StatsCounter(byPrintingAuthorityAndCountingCircle)

    when:
    counter.append(newVoterForPrinterAndCountingCircle("printer1", 1))
    counter.append(newVoterForPrinterAndCountingCircle("printer1", 1))
    counter.append(newVoterForPrinter("printer2"))
    counter.append(newVoterForPrinterAndCountingCircle("printer1", 2))
    counter.append(newVoterForPrinter("printer1"))
    counter.append(newVoterForPrinterAndCountingCircle("printer1", 2))

    then:
    counter.numbers.size() == 4
    counter.getCountFor(["printer1", 1]) == 2
    counter.getCountFor(["printer1", 2]) == 2
    counter.getCountFor(["printer1", null]) == 1
    counter.getCountFor(["printer2", null]) == 1
  }

  def "GetCountFor absent mapping returns 0"() {
    given:
    def counter = new StatsCounter(byPrintingAuthority)
    counter.append(newVoterForPrinter("printer1"))

    when:
    def count = counter.getCountFor("printer2")

    then:
    count == 0
  }

  def "GetCountFor with a non equals-matching value returns 0"() {
    given:
    def counter = new StatsCounter(byPrintingAuthorityAndCountingCircle)
    counter.append(newVoterForPrinterAndCountingCircle("printer1", 1))

    when:
    def count = counter.getCountFor([1, "printer1"])  // reversed values order

    then:
    count == 0
  }

  def "getTotal should sum all aggregates"() {
    given:
    def counter = new StatsCounter(byPrintingAuthority)
    counter.append(newVoterForPrinter("printer1"))
    counter.append(newVoterForPrinter("printer1"))
    counter.append(newVoterForPrinter("printer2"))

    when:
    def total = counter.getTotal()

    then:
    total == 3
  }

  def "getTotal should be 0 before any item counted"() {
    given:
    def counter = new StatsCounter(byPrintingAuthority)

    when:
    def total = counter.getTotal()

    then:
    total == 0
  }

  def "applyTo should check the whole collection"() {
    given:
    def counter = new StatsCounter(byPrintingAuthority)
    def collection = [
            newVoterForPrinter("printer1"),
            newVoterForPrinter("printer1"),
            newVoterForPrinter("printer2"),
            newVoterForPrinter("printer1")
    ]

    when:
    counter.applyTo(collection)

    then:
    counter.numbers.size() == 2
    counter.getCountFor("printer1") == 3
  }

  def "asMap should expose all results as a Map"() {
    given:
    def counter = new StatsCounter(byPrintingAuthority)
    counter.applyTo([
            newVoterForPrinter("printer1"),
            newVoterForPrinter("printer1"),
            newVoterForPrinter("printer2"),
            newVoterForPrinter("printer2"),
            newVoterForPrinter("printer1")
    ])

    when:
    def map = counter.asMap()

    then:
    map == ["printer1": 3, "printer2": 2]
  }

  def "asAggregatedMap should aggregate all values in the resulting Map"() {
    given:
    def counter = new StatsCounter(byPrintingAuthorityAndCountingCircle)
    counter.applyTo([
            newVoterForPrinterAndCountingCircle("printer1", 1),
            newVoterForPrinterAndCountingCircle("printer1", 2),
            newVoterForPrinterAndCountingCircle("printer2", 1),
            newVoterForPrinterAndCountingCircle("printer2", 2),
            newVoterForPrinterAndCountingCircle("printer1", 1)
    ])

    when: "We generate both maps for the comparison"
    def map = counter.asMap()
    def aggregatedMap = counter.asAggregatedMap({ it.first() })

    then:
    map == [["printer1", 1]: 2, ["printer1", 2]: 1, ["printer2", 1]: 1, ["printer2", 2]: 1]
    aggregatedMap == ["printer1": 3, "printer2": 2]
  }
}
