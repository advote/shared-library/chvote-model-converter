/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.model

import java.time.LocalDate
import java.time.Month
import java.time.Year
import java.time.YearMonth
import spock.lang.Specification

class PartialLocalDateTest extends Specification {

  def "it should be possible to create a partial date from a year"() {
    given:
    def year = Year.of(2018)

    when:
    def partialDate = new PartialLocalDate(year)

    then:
    partialDate.type == PartialLocalDate.Type.YEAR
    partialDate.year == year
    partialDate.monthOfYear.empty()
    partialDate.dayOfMonth.empty()
  }

  def "it should be possible to create a partial date from a year and a month"() {
    given:
    def year = 1984
    def month = Month.APRIL

    when:
    def partialDate = new PartialLocalDate(YearMonth.of(year, month))

    then:
    partialDate.type == PartialLocalDate.Type.YEAR_MONTH
    partialDate.year == Year.of(year)
    partialDate.monthOfYear == Optional.of(month)
    partialDate.dayOfMonth.empty()
  }

  def "it should be possible to create a partial date from a local date"() {
    given:
    def year = 2020
    def month = Month.FEBRUARY
    def day = 29

    when:
    def partialDate = new PartialLocalDate(LocalDate.of(year, month, day))

    then:
    partialDate.type == PartialLocalDate.Type.YEAR_MONTH_DAY
    partialDate.year == Year.of(year)
    partialDate.monthOfYear == Optional.of(month)
    partialDate.dayOfMonth == Optional.of(day)
  }
}