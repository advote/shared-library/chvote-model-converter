/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl.ech0157.model;

import ch.ge.ve.interfaces.ech.eCH0155.v4.CandidatePositionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ListType;
import ch.ge.ve.interfaces.ech.eCH0157.v4.EventInitialDelivery.ElectionGroupBallot.ElectionInformation;
import java.util.Optional;

/**
 * An {@link ElectionInformation} decorator.
 */
public class ECH0157ElectionInformation {
  private static final Integer PROPORTIONAL_ELECTION_TYPE = 1;
  private static final Integer MAJORITY_ELECTION_TYPE     = 2;

  private final ElectionInformation electionInformation;

  /**
   * Create a new eCH-0157 election information decorator.
   *
   * @param electionInformation the {@link ElectionInformation} to decorate.
   */
  public ECH0157ElectionInformation(ElectionInformation electionInformation) {
    this.electionInformation = electionInformation;
  }

  /**
   * Whether this is a proportional election.
   *
   * @return true if it's a proportional election, false otherwise.
   */
  public boolean isProportionalElection() {
    return PROPORTIONAL_ELECTION_TYPE.equals(electionInformation.getElection().getTypeOfElection().intValue());
  }

  /**
   * Whether this is a majority election.
   *
   * @return true if it's a majority election, false otherwise.
   */
  public boolean isMajorityElection() {
    return MAJORITY_ELECTION_TYPE.equals(electionInformation.getElection().getTypeOfElection().intValue());
  }

  /**
   * Get the identification string of the decorated {@link ElectionInformation}. Same as calling:
   * <code>
   * electionInformation.getElection().getElectionIdentification();
   * </code>
   *
   * @return the election identification string.
   */
  public String getElectionIdentification() {
    return electionInformation.getElection().getElectionIdentification();
  }

  /**
   * Find the first electoral roll id where the given candidate is present.
   *
   * @param candidateId the candidate id.
   *
   * @return the electoral roll id.
   */
  public Optional<String> findElectionRollByCandidateId(String candidateId) {
    return electionInformation
        .getList()
        .stream()
        .filter(electoralRoll ->
                    electoralRoll.getCandidatePosition()
                                 .stream()
                                 .map(
                                     CandidatePositionInformationType::getCandidateIdentification)
                                 .anyMatch(candidateId::equals)
        )
        .map(ListType::getListIdentification)
        .findFirst();
  }
}
