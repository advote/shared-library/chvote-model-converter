/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl;

import ch.ge.ve.interfaces.ech.eCH0044.v4.DatePartiallyKnownType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.AuthorityType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.PersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0058.v5.SendingApplicationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.CountingCircleType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceType;
import ch.ge.ve.model.convert.api.PrinterMappingMismatchException;
import ch.ge.ve.model.convert.api.VoterConverter;
import ch.ge.ve.model.convert.model.PartialLocalDate;
import ch.ge.ve.model.convert.model.VoterDetails;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.XMLEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation class for {@link VoterConverter}.
 */
public class DefaultVoterConverter implements VoterConverter {
  private static final Logger log = LoggerFactory.getLogger(DefaultVoterConverter.class);

  private static final String VOTER               = "voter";
  private static final String REPORTING_AUTHORITY = "reportingAuthority";
  private static final String DELIVERY_HEADER     = "deliveryHeader";

  private static final String ECH_0058_NS = "http://www.ech.ch/xmlns/eCH-0058/5";
  private static final String ECH_0045_NS = "http://www.ech.ch/xmlns/eCH-0045/4";
  private static final String ECH_0044_NS = "http://www.ech.ch/xmlns/eCH-0044/4";
  private static final String ECH_0005_NS = "http://www.ech.ch/xmlns/eCH-0007/6";
  private static final String ECH_0010_NS = "http://www.ech.ch/xmlns/eCH-0010/6";
  private static final String ECH_0155_NS = "http://www.ech.ch/xmlns/eCH-0155/3";

  private static final String ECH_0058_P = "e58";
  private static final String ECH_0045_P = "e45";
  private static final String ECH_0044_P = "e44";
  private static final String ECH_0005_P = "e5";
  private static final String ECH_0010_P = "e10";
  private static final String ECH_0155_P = "e155";

  private final JAXBContext      jaxbContext;
  private final VoterIdGenerator voterIdGenerator;

  public DefaultVoterConverter(VoterIdGenerator voterIdGenerator) {
    try {
      this.jaxbContext = JAXBContext.newInstance(VotingPersonType.class, AuthorityType.class, HeaderType.class);
      this.voterIdGenerator = voterIdGenerator;
    } catch (JAXBException e) {
      log.error("Failed to instantiate DefaultVoterConverter", e);
      throw new IllegalStateException("Failed to instantiate JAXBContext for DefaultVoterConverter", e);
    }
  }

  private Marshaller createMarshaller() throws JAXBException {
    final Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
    return marshaller;
  }

  @Override
  public Stream<VoterDetails> convertToVoterList(Map<Integer, String> printingAuthorityByMunicipality,
                                          Map<String, String> printingAuthoritiesByCanton,
                                          InputStream[] voterDeliveries) {
    final VoterMappingContext voterMappingContext = new VoterMappingContext();

    return Arrays.stream(voterDeliveries).parallel()
                 .flatMap(deliveryStream -> new VoterIteratorVoterImpl(
                     deliveryStream,
                     printingAuthorityByMunicipality,
                     printingAuthoritiesByCanton,
                     voterMappingContext)
                     .toStream()
                 );
  }

  @Override
  public void remapByPrinter(Map<String, OutputStream> voterDeliveriesByPrinters,
                             Map<Integer, String> printingAuthorityByMunicipality,
                             Map<String, String> printingAuthorityByCanton,
                             Map<String, Long> numberOfVotersByPrinter,
                             InputStream[] voterDeliveries) {

    checkOutputStreamsPrinters(voterDeliveriesByPrinters, numberOfVotersByPrinter);

    Map<String, XMLStreamWriter> streamWriters = new HashMap<>();

    for (InputStream voterDelivery : voterDeliveries) {
      VoterIteratorVotingPersonTypeImpl voterIterator =
          new VoterIteratorVotingPersonTypeImpl(voterDelivery,
                                                printingAuthorityByMunicipality,
                                                printingAuthorityByCanton);
      while (voterIterator.hasNext()) {
        remapVoter(voterDeliveriesByPrinters,
                   printingAuthorityByMunicipality,
                   printingAuthorityByCanton,
                   numberOfVotersByPrinter,
                   streamWriters,
                   voterIterator.next());
      }
    }

    finalizeRegistries(streamWriters);
  }

  private void checkOutputStreamsPrinters(Map<String, OutputStream> voterDeliveriesByPrinters,
                                          Map<String, Long> numberOfVotersByPrinter) {
    // check that there is an output stream for each printer
    if (!numberOfVotersByPrinter.keySet().equals(voterDeliveriesByPrinters.keySet())) {
      throw new PrinterMappingMismatchException(
          String.format("There should be the same printers for the output streams (%s) " +
                        "than the ones found for the voters deliveries (%s)",
                        voterDeliveriesByPrinters.keySet().toString(),
                        numberOfVotersByPrinter.keySet().toString())
      );
    }
  }

  private void remapVoter(Map<String, OutputStream> voterDeliveriesByPrinters,
                          Map<Integer, String> printingAuthorityByMunicipality,
                          Map<String, String> printingAuthorityByCanton,
                          Map<String, Long> numberOfVotersByPrinter,
                          Map<String, XMLStreamWriter> streamWriters,
                          VotingPersonType votingPerson) {

    VoterInformationExtractor voterInformationExtractor = new VoterInformationExtractor(
        votingPerson,
        printingAuthorityByMunicipality,
        printingAuthorityByCanton);

    XMLStreamWriter writer = streamWriters.computeIfAbsent(
        voterInformationExtractor.getPrintingAuthorityName(),
        printer -> createStreamWriter(printer, voterDeliveriesByPrinters, numberOfVotersByPrinter)
    );

    try {
      QName qName = new QName(ECH_0045_NS, VOTER, ECH_0045_P);
      JAXBElement<VotingPersonType> element = new JAXBElement<>(qName, VotingPersonType.class, votingPerson);
      createMarshaller().marshal(element, writer);
    } catch (JAXBException e) {
      throw new IllegalStateException("Cannot serialize votingPerson", e);
    }
  }

  private void finalizeRegistries(Map<String, XMLStreamWriter> streamWriters) {
    streamWriters.values().forEach(writer -> {
      try {
        writer.writeEndElement(); // voterList
        writer.writeEndElement(); // voterDelivery
        writer.writeEndDocument();
        writer.close();
      } catch (XMLStreamException e) {
        throw new IllegalStateException("Cannot close writer", e);
      }
    });
  }

  private XMLStreamWriter createStreamWriter(String printer,
                                             Map<String, OutputStream> voterDeliveriesByPrinters,
                                             Map<String, Long> numberOfVotersByPrinter) {
    OutputStream outputStream = voterDeliveriesByPrinters.get(printer);
    if (outputStream == null) {
      throw new IllegalStateException(String.format("Cannot find an output stream for %s", printer));
    }

    XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
    XMLStreamWriter writer;
    try {
      writer = xmlOutputFactory.createXMLStreamWriter(outputStream, StandardCharsets.UTF_8.name());
      writer.writeStartDocument("utf-8", "1.0");

      writer.setPrefix(ECH_0058_P, ECH_0058_NS);
      writer.setPrefix(ECH_0045_P, ECH_0045_NS);
      writer.setPrefix(ECH_0044_P, ECH_0044_NS);
      writer.setPrefix(ECH_0005_P, ECH_0005_NS);
      writer.setPrefix(ECH_0010_P, ECH_0010_NS);
      writer.setPrefix(ECH_0155_P, ECH_0155_NS);

      writer.writeStartElement(ECH_0045_NS, "voterDelivery");
      writer.writeNamespace(ECH_0058_P, ECH_0058_NS);
      writer.writeNamespace(ECH_0045_P, ECH_0045_NS);
      writer.writeNamespace(ECH_0044_P, ECH_0044_NS);
      writer.writeNamespace(ECH_0005_P, ECH_0005_NS);
      writer.writeNamespace(ECH_0010_P, ECH_0010_NS);
      writer.writeNamespace(ECH_0155_P, ECH_0155_NS);

      writeDeliveryHeader(writer);

      writer.writeStartElement(ECH_0045_NS, "voterList");
      {
        writer.writeStartElement(ECH_0045_NS, REPORTING_AUTHORITY);
        {
          writer.writeStartElement(ECH_0045_NS, "otherRegister");
          {
            writer.writeStartElement(ECH_0045_NS, "registerIdentification");
            writer.writeCharacters(UUID.randomUUID().toString());
            writer.writeEndElement();
            writer.writeStartElement(ECH_0045_NS, "registerName");
            writer.writeCharacters("Aggregated register for " + printer);
            writer.writeEndElement();
          }
          writer.writeEndElement();
        }
        writer.writeEndElement();

        writer.writeStartElement(ECH_0045_NS, "numberOfVoters");
        writer.writeCharacters(numberOfVotersByPrinter.getOrDefault(printer, 0L).toString());
        writer.writeEndElement();
      }

    } catch (XMLStreamException e) {
      throw new IllegalStateException("Cannot initialize document", e);
    }

    return writer;
  }

  private void writeDeliveryHeader(XMLStreamWriter writer) throws XMLStreamException {
    writer.writeStartElement(ECH_0045_NS, DELIVERY_HEADER);
    {
      writer.writeStartElement(ECH_0058_NS, "senderId");
      writer.writeCharacters("admin.chvote.ch");
      writer.writeEndElement();
    }
    {
      writer.writeStartElement(ECH_0058_NS, "messageId");
      writer.writeCharacters(UUID.randomUUID().toString());
      writer.writeEndElement();
    }
    {
      writer.writeStartElement(ECH_0058_NS, "messageType");
      writer.writeCharacters("Registry");
      writer.writeEndElement();
    }
    {
      writer.writeStartElement(ECH_0058_NS, "sendingApplication");
      {
        writer.writeStartElement(ECH_0058_NS, "manufacturer");
        writer.writeCharacters("OCSIN - Etat de Genève");
        writer.writeEndElement();
      }
      {
        writer.writeStartElement(ECH_0058_NS, "product");
        writer.writeCharacters("CHVote");
        writer.writeEndElement();
      }
      {
        writer.writeStartElement(ECH_0058_NS, "productVersion");
        writer.writeCharacters("1");
        writer.writeEndElement();
      }
      writer.writeEndElement();
      {
        writer.writeStartElement(ECH_0058_NS, "messageDate");
        writer.writeCharacters(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        writer.writeEndElement();
      }
      {
        writer.writeStartElement(ECH_0058_NS, "action");
        writer.writeCharacters("1");
        writer.writeEndElement();
      }
      {
        writer.writeStartElement(ECH_0058_NS, "testDeliveryFlag");
        writer.writeCharacters("false");
        writer.writeEndElement();
      }
    }
    writer.writeEndElement();
  }

  private DomainOfInfluence convertToDomainOfInfluence(VotingPersonType.DomainOfInfluenceInfo domainOfInfluenceInfo) {
    DomainOfInfluenceType domainOfInfluence = domainOfInfluenceInfo.getDomainOfInfluence();
    return new DomainOfInfluence(domainOfInfluence.getLocalDomainOfInfluenceIdentification());
  }

  private class VoterMappingContext {
    private final AtomicInteger                         voterCount                 = new AtomicInteger(0);
    private final AtomicInteger                         countingCircleCount        = new AtomicInteger(0);
    private final ConcurrentMap<String, CountingCircle> countingCircleByBusinessId = new ConcurrentHashMap<>();
  }

  private abstract class VoterIterator<T> implements Iterator<T> {
    private final XMLEventReader       eventReader;
    private final Map<Integer, String> printingAuthorityByMunicipality;
    private final Map<String, String>  printingAuthorityByCanton;

    String registerId;
    String deliveryId;
    private final Unmarshaller unmarshaller;

    VoterIterator(InputStream deliveryStream,
                  Map<Integer, String> printingAuthorityByMunicipality,
                  Map<String, String> printingAuthorityByCanton) {
      try {
        this.eventReader = XMLInputFactory.newFactory().createXMLEventReader(deliveryStream);
      } catch (XMLStreamException e) {
        log.error("Failed to parse input stream", e);
        throw new IllegalStateException(e);
      }
      this.printingAuthorityByMunicipality = printingAuthorityByMunicipality;
      this.printingAuthorityByCanton = printingAuthorityByCanton;
      try {
        unmarshaller = jaxbContext.createUnmarshaller();
      } catch (JAXBException e) {
        throw new IllegalStateException("Failed to initialize unmarshaller", e);
      }
    }

    Stream<T> toStream() {
      int characteristics = Spliterator.DISTINCT | Spliterator.ORDERED;
      Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(this, characteristics);
      return StreamSupport.stream(spliterator, false);
    }

    @Override
    public boolean hasNext() {
      try {
        while (eventReader.hasNext()) {
          XMLEvent event = eventReader.peek();
          if (event.isEndDocument()) {
            return false;
          } else if (event.isStartElement() && DELIVERY_HEADER
              .equals(event.asStartElement().getName().getLocalPart())) {
            extractDeliveryId(eventReader);
          } else if (event.isStartElement() && REPORTING_AUTHORITY
              .equals(event.asStartElement().getName().getLocalPart())) {
            extractRegisterId(eventReader);
          } else if (event.isStartElement() && VOTER.equals(event.asStartElement().getName().getLocalPart())) {
            return true;
          } else {
            eventReader.nextEvent();
          }
        }
        return false;
      } catch (JAXBException | XMLStreamException e) {
        log.error("Failed to parse the xml stream", e);
        throw new IllegalArgumentException("An invalid stream was supplied", e);
      }
    }

    @Override
    public T next() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }
      try {
        JAXBElement<VotingPersonType> element = unmarshaller.unmarshal(eventReader, VotingPersonType.class);
        return map(element.getValue(), printingAuthorityByMunicipality, printingAuthorityByCanton);
      } catch (JAXBException e) {
        log.error("Failed to read a valid VotingPersonType from the xml stream", e);
        throw new IllegalArgumentException("An invalid stream was supplied", e);
      }
    }

    protected abstract T map(VotingPersonType value,
                             Map<Integer, String> printingAuthorityByMunicipality,
                             Map<String, String> printingAuthorityByCanton);

    private void extractRegisterId(XMLEventReader eventReader) throws JAXBException {
      JAXBElement<AuthorityType> element = unmarshaller.unmarshal(eventReader, AuthorityType.class);
      AuthorityType value = element.getValue();
      if (value.getCantonalRegister() != null) {
        registerId = value.getCantonalRegister().getRegisterIdentification();
      } else if (value.getMunicipalityRegister() != null) {
        registerId = value.getMunicipalityRegister().getRegisterIdentification();
      } else if (value.getOtherRegister() != null) {
        registerId = value.getOtherRegister().getRegisterIdentification();
      } else {
        throw new IllegalArgumentException("The authority type must contain one of the xsd defined registers");
      }
    }

    private void extractDeliveryId(XMLEventReader eventReader) throws JAXBException {
      JAXBElement<HeaderType> element = unmarshaller.unmarshal(eventReader, HeaderType.class);
      HeaderType value = element.getValue();
      String messageId = value.getMessageId();
      String messageDate = value.getMessageDate().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
      String senderId = value.getSenderId();
      SendingApplicationType sendingApplication = value.getSendingApplication();
      String manufacturer = sendingApplication.getManufacturer();
      String product = sendingApplication.getProduct();
      deliveryId = String.format("%s-%s-%s-%s-%s", senderId, manufacturer, product, messageDate, messageId);
    }
  }

  private class VoterIteratorVoterImpl extends VoterIterator<VoterDetails> {

    private final VoterMappingContext voterMappingContext;

    VoterIteratorVoterImpl(InputStream deliveryStream,
                           Map<Integer, String> printingAuthorityByMunicipality,
                           Map<String, String> printingAuthorityByCanton,
                           VoterMappingContext voterMappingContext) {
      super(deliveryStream, printingAuthorityByMunicipality, printingAuthorityByCanton);
      this.voterMappingContext = voterMappingContext;
    }

    @Override
    protected VoterDetails map(VotingPersonType votingPerson,
                        Map<Integer, String> printingAuthorityByMunicipality,
                        Map<String, String> printingAuthorityByCanton) {
      VoterInformationExtractor voterInformationExtractor = new VoterInformationExtractor(
          votingPerson, printingAuthorityByMunicipality, printingAuthorityByCanton);
      PersonType person = voterInformationExtractor.getPersonType();

      String printingAuthorityName = voterInformationExtractor.getPrintingAuthorityName();

      String personId = person.getPersonIdentification().getLocalPersonId().getPersonId();
      DatePartiallyKnownType dateOfBirth = person.getPersonIdentification().getDateOfBirth();
      String businessId = voterIdGenerator.generateId(deliveryId, registerId, personId);
      return new VoterDetails(
          voterMappingContext.voterCount.getAndIncrement(), businessId,
          toPartialLocalDate(dateOfBirth), getCountingCircle(votingPerson),
          printingAuthorityName,
          votingPerson.getDomainOfInfluenceInfo().stream()
                      .map(DefaultVoterConverter.this::convertToDomainOfInfluence)
                      .collect(Collectors.toList())
      );
    }

    private PartialLocalDate toPartialLocalDate(DatePartiallyKnownType date) {
      if (date.getYearMonthDay() != null) {
        return new PartialLocalDate(date.getYearMonthDay());
      } else if (date.getYearMonth() != null) {
        XMLGregorianCalendar yearMonth = date.getYearMonth();
        return new PartialLocalDate(YearMonth.of(yearMonth.getYear(), yearMonth.getMonth()));
      } else if (date.getYear() != null) {
        return new PartialLocalDate(Year.of(date.getYear().getYear()));
      }
      throw new IllegalArgumentException("Invalid date");
    }

    private CountingCircle getCountingCircle(VotingPersonType person) {
      List<CountingCircleType> countingCircles = person.getDomainOfInfluenceInfo().stream()
                                                       .map(VotingPersonType.DomainOfInfluenceInfo::getCountingCircle)
                                                       .filter(Objects::nonNull).collect(Collectors.toList());
      if (countingCircles.stream().map(CountingCircleType::getCountingCircleId).distinct().count() != 1) {
        throw new IllegalArgumentException("A voting person must be bound to exactly one counting circle.");
      }
      CountingCircleType countingCircle =
          countingCircles.stream().findFirst()
                         .orElseThrow(() -> new IllegalArgumentException(
                             "A voting person must have at least one counting circle"));

      if (countingCircle.getCountingCircleId() == null) {
        throw new IllegalArgumentException("A counting circle must have an id.");
      }

      return voterMappingContext.countingCircleByBusinessId.computeIfAbsent(
          countingCircle.getCountingCircleId(),
          countingCircleId -> new CountingCircle(voterMappingContext.countingCircleCount.getAndIncrement(),
                                                 countingCircleId,
                                                 countingCircle.getCountingCircleName()));
    }
  }

  private class VoterIteratorVotingPersonTypeImpl extends VoterIterator<VotingPersonType> {
    VoterIteratorVotingPersonTypeImpl(InputStream deliveryStream,
                                      Map<Integer, String> printingAuthorityByMunicipality,
                                      Map<String, String> printingAuthorityByCanton) {
      super(deliveryStream, printingAuthorityByMunicipality, printingAuthorityByCanton);
    }

    @Override
    protected VotingPersonType map(VotingPersonType votingPersonType,
                                   Map<Integer, String> printingAuthorityByMunicipality,
                                   Map<String, String> printingAuthorityByCanton) {
      VoterInformationExtractor voterExtractor = new VoterInformationExtractor(votingPersonType,
                                                                               printingAuthorityByMunicipality,
                                                                               printingAuthorityByCanton);
      PersonType person = voterExtractor.getPersonType();
      String personId = person.getPersonIdentification().getLocalPersonId().getPersonId();
      String voterId = voterIdGenerator.generateId(deliveryId, registerId, personId);
      person.getPersonIdentification().getLocalPersonId().setPersonId(voterId);
      return votingPersonType;
    }
  }
}

