/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl;

import ch.ge.ve.interfaces.ech.eCH0155.v4.AnswerInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType;
import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery;
import ch.ge.ve.model.convert.api.VotationConverter;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import ch.ge.ve.model.convert.model.ElectionDefinition;
import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import ch.ge.ve.protocol.model.Election;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation class for {@link VotationConverter}.
 */
public class DefaultVotationConverter implements VotationConverter {

  private static final Logger log                = LoggerFactory.getLogger(DefaultVoterConverter.class);
  private static final int    SELECTIONS_ALLOWED = 1;

  private final Map<String, DomainOfInfluence> doiById = new ConcurrentHashMap<>();
  private final JAXBContext                    jaxbContext;

  public DefaultVotationConverter() {
    try {
      this.jaxbContext = JAXBContext.newInstance(Delivery.class);
    } catch (JAXBException e) {
      log.error("Failed to instantiate DefaultVotationConverter", e);
      throw new IllegalStateException("Failed to instantiate JAXBContext for DefaultVotationConverter", e);
    }
  }

  @Override
  public List<ElectionDefinition> convertToElectionDefinitionList(InputStream... votationDeliveryStreams) {
    return Arrays.stream(votationDeliveryStreams)
                 .flatMap(this::unmarshallAndConvert)
                 .collect(Collectors.toList());
  }

  private Stream<ElectionDefinition> unmarshallAndConvert(InputStream votationDeliveryStream) {
    try {
      XMLEventReader eventReader = XMLInputFactory.newFactory().createXMLEventReader(votationDeliveryStream);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      JAXBElement<Delivery> element = unmarshaller.unmarshal(eventReader, Delivery.class);
      return convertDelivery(element.getValue());
    } catch (XMLStreamException | JAXBException e) {
      log.error("Failed to parse input stream", e);
      throw new IllegalArgumentException("Invalid eCH-0159 stream", e);
    }
  }

  private Stream<ElectionDefinition> convertDelivery(Delivery delivery) {
    return delivery.getInitialDelivery().getVoteInformation().stream()
                   .flatMap(this::convertVoteInformation);
  }

  private Stream<ElectionDefinition> convertVoteInformation(EventInitialDelivery.VoteInformation voteInformation) {
    String domainOfInfluenceId = voteInformation.getVote().getDomainOfInfluenceIdentification();
    DomainOfInfluence domainOfInfluence = doiById.computeIfAbsent(domainOfInfluenceId, DomainOfInfluence::new);
    return voteInformation.getBallot().stream().flatMap(ballotType -> convertBallot(ballotType, domainOfInfluence));
  }

  private Stream<ElectionDefinition> convertBallot(BallotType ballotType, DomainOfInfluence domainOfInfluence) {
    if (ballotType.getStandardBallot() != null) {
      return Stream.of(convertStandardBallot(ballotType.getStandardBallot(), domainOfInfluence));
    } else if (ballotType.getVariantBallot() != null) {
      return convertVariantBallot(ballotType.getVariantBallot(), domainOfInfluence);
    } else {
      throw new IllegalArgumentException(
          "Neither standard ballot nor variant ballot defined for " + ballotType.getBallotIdentification());
    }
  }

  private ElectionDefinition convertStandardBallot(BallotType.StandardBallot ballot,
                                                   DomainOfInfluence domainOfInfluence) {
    String questionIdentification = ballot.getQuestionIdentification();
    AnswerInformationType answerType = ballot.getAnswerInformation();
    return buildElection(domainOfInfluence, questionIdentification, answerType);
  }

  private Stream<ElectionDefinition> convertVariantBallot(BallotType.VariantBallot ballot,
                                                          DomainOfInfluence domainOfInfluence) {
    return Stream.concat(
        ballot.getQuestionInformation().stream()
              .map(questionInformationType -> convertQuestionInformation(questionInformationType, domainOfInfluence)),
        ballot.getTieBreakInformation().stream()
              .map(tieBreakInformationType -> convertTieBreakInformation(tieBreakInformationType, domainOfInfluence)));
  }

  private ElectionDefinition convertQuestionInformation(QuestionInformationType questionInformationType,
                                                        DomainOfInfluence domainOfInfluence) {
    String questionIdentification = questionInformationType.getQuestionIdentification();
    AnswerInformationType answerType = questionInformationType.getAnswerInformation();
    return buildElection(domainOfInfluence, questionIdentification, answerType);
  }

  private ElectionDefinition convertTieBreakInformation(TieBreakInformationType tieBreakInformationType,
                                                        DomainOfInfluence domainOfInfluence) {
    String questionIdentification = tieBreakInformationType.getQuestionIdentification();
    AnswerInformationType answerType = tieBreakInformationType.getAnswerInformation();
    return buildElection(domainOfInfluence, questionIdentification, answerType);
  }

  private ElectionDefinition buildElection(DomainOfInfluence domainOfInfluence,
                                           String questionIdentification, AnswerInformationType answerType) {
    List<Candidate> candidates = createCandidatesForAnswerType(answerType, questionIdentification);
    Election election = new Election(questionIdentification, candidates.size(), SELECTIONS_ALLOWED, domainOfInfluence);
    return new ElectionDefinition(election, candidates);
  }

  private List<Candidate> createCandidatesForAnswerType(AnswerInformationType answerType,
                                                        String questionIdentification) {
    AnswerTypeEnum answerTypeEnum = AnswerTypeEnum.fromInt(answerType.getAnswerType().intValue());
    return answerTypeEnum.getOptions().stream()
                         .map(optionName -> new Candidate(questionIdentification + ":" + optionName))
                         .collect(Collectors.toList());
  }
}
