/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl;

import ch.ge.ve.interfaces.ech.eCH0045.v4.ForeignerType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.PersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissAbroadType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissDomesticType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.model.convert.api.PrinterMappingMismatchException;
import java.util.Map;
import java.util.Optional;

/**
 * Extract attributes of a voter from the XML voter node
 */
class VoterInformationExtractor {
  private final Map<Integer, String> printingAuthorityByMunicipality;
  private final Map<String, String>  printingAuthorityByCanton;
  private       PersonType           personType;
  private       Integer              printingAuthorityMunicipalityKey;
  private       String               printingAuthorityCantonalKey;

  VoterInformationExtractor(VotingPersonType votingPersonType,
                            Map<Integer, String> printingAuthorityByMunicipality,
                            Map<String, String> printingAuthorityByCanton) {
    this.printingAuthorityByMunicipality = printingAuthorityByMunicipality;
    this.printingAuthorityByCanton = printingAuthorityByCanton;

    VotingPersonType.Person person = votingPersonType.getPerson();
    if (person.getSwiss() != null) {
      handleSwissDomestic(person.getSwiss());
    } else if (person.getForeigner() != null) {
      handleForeigner(person.getForeigner());
    } else if (person.getSwissAbroad() != null) {
      handleSwissAbroad(person.getSwissAbroad());
    } else {
      throw new IllegalArgumentException("person doesn't match a known type");
    }
  }

  private void handleSwissAbroad(SwissAbroadType swissAbroad) {
    personType = swissAbroad.getSwissAbroadPerson();
    if (swissAbroad.getMunicipality() != null) {
      printingAuthorityMunicipalityKey = swissAbroad.getMunicipality().getMunicipalityId();
    } else {
      printingAuthorityCantonalKey = swissAbroad.getCanton().value();
    }
  }

  private void handleForeigner(ForeignerType foreigner) {
    personType = foreigner.getForeignerPerson();
    printingAuthorityMunicipalityKey = foreigner.getMunicipality().getMunicipalityId();
  }

  private void handleSwissDomestic(SwissDomesticType swiss) {
    personType = swiss.getSwissDomesticPerson();
    printingAuthorityMunicipalityKey = swiss.getMunicipality().getMunicipalityId();
  }

  PersonType getPersonType() {
    return personType;
  }

  private Optional<Integer> getPrintingAuthorityMunicipalityKey() {
    return Optional.ofNullable(printingAuthorityMunicipalityKey);
  }

  private Optional<String> getPrintingAuthorityCantonalKey() {
    return Optional.ofNullable(printingAuthorityCantonalKey);
  }

  String getPrintingAuthorityName() {
    return getPrintingAuthorityMunicipalityKey()
        .map(printingAuthorityByMunicipality::get)
        .orElseGet(() -> getPrintingAuthorityCantonalKey()
            .map(printingAuthorityByCanton::get)
            .orElseThrow(() -> new PrinterMappingMismatchException(
                String.format(
                    "neither municipality id [%s] nor canton [%s] match the printer mapping for voter [%s]: " +
                    "municipalities mapping=[%s], " +
                    "canton mapping=[%s]",
                    getPrintingAuthorityMunicipalityKey().orElse(null),
                    getPrintingAuthorityCantonalKey().orElse(null),
                    personType.getPersonIdentification().getLocalPersonId().getPersonId(),
                    printingAuthorityByMunicipality,
                    printingAuthorityByCanton)
            ))
        );
  }
}
