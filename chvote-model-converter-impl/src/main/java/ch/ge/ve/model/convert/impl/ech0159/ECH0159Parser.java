/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl.ech0159;

import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery;
import ch.ge.ve.model.convert.impl.ech0159.model.ECH0159VoteInformation;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An object to parse an operation reference eCH-0159 XML file int a list of {@link ECH0159VoteInformation}.
 */
public class ECH0159Parser {
  private static final Logger log = LoggerFactory.getLogger(ECH0159Parser.class);

  private final JAXBContext     jaxbContext;
  private final XMLInputFactory xmlInputFactory;

  /**
   * Create a new eCH-0159 parser.
   */
  public ECH0159Parser() {
    try {
      this.jaxbContext = JAXBContext.newInstance(Delivery.class);
      this.xmlInputFactory = XMLInputFactory.newFactory();
    } catch (Exception e) {
      log.error("Failed to instantiate ECH0159Parser", e);
      throw new IllegalStateException("Failed to instantiate JAXB elements for ECH0159Parser", e);
    }
  }

  private List<ECH0159VoteInformation> retrieveVoteInformation(InputStream votationDeliveryStream) {
    try {
      XMLEventReader reader = xmlInputFactory.createXMLEventReader(votationDeliveryStream);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      Delivery delivery = unmarshaller.unmarshal(reader, Delivery.class).getValue();
      return delivery.getInitialDelivery().getVoteInformation()
                     .stream().map(ECH0159VoteInformation::new)
                     .collect(Collectors.toList());
    } catch (XMLStreamException | JAXBException e) {
      throw new IllegalArgumentException("Invalid eCH-0159 stream", e);
    }
  }

  /**
   * Retrieve a list of vote information for all the given ECH0159 delivery streams.
   *
   * @param votationDeliveryStreams the ECH0159 delivery streams.
   *
   * @return the list of {@link ECH0159VoteInformation} as they appear in the files.
   */
  public List<ECH0159VoteInformation> retrieveVoteInformation(InputStream... votationDeliveryStreams) {
    return Arrays.stream(votationDeliveryStreams)
                 .map(this::retrieveVoteInformation)
                 .flatMap(List::stream)
                 .collect(Collectors.toList());
  }

  /**
   * Works as {@link #retrieveVoteInformation(InputStream...)} but the input is the content of a single delivery as a
   * byte array.
   *
   * @see #retrieveVoteInformation(InputStream...)
   */
  public List<ECH0159VoteInformation> retrieveVoteInformation(byte[] votationDelivery) {
    try (InputStream in = new ByteArrayInputStream(votationDelivery)) {
      return retrieveVoteInformation(in);
    } catch (IOException e) {
      throw new IllegalArgumentException("Invalid eCH-0159 stream", e);
    }
  }
}
