/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;

/**
 * The enumeration of all delivery types for both election and votation deliveries.
 */
public enum DeliveryType {
  /**
   * The eCH-0157 delivery type.
   */
  ECH_0157("http://www.ech.ch/xmlns/eCH-0157/4"),
  /**
   * The eCH-0159 delivery type.
   */
  ECH_0159("http://www.ech.ch/xmlns/eCH-0159/4");

  private final String schemaLocation;

  DeliveryType(String schemaLocation) {
    this.schemaLocation = schemaLocation;
  }

  /**
   * Resolve the delivery type for the given content.
   *
   * @param deliveryContent the content of the file.
   * @param charset         the charset of the content.
   *
   * @return the resolved delivery type, can be null if none was found.
   */
  public static Optional<DeliveryType> getDeliveryType(byte[] deliveryContent, Charset charset) {
    String contentAsString = new String(deliveryContent, charset);

    return Arrays.stream(DeliveryType.values())
                 .filter(deliveryType -> contentAsString.contains(deliveryType.schemaLocation))
                 .findFirst();
  }

  /**
   * Resolve the delivery type for the given content.
   *
   * @param deliveryContent the content of the file.
   *
   * @return the resolved delivery type, can be null if none was found.
   **/
  public static Optional<DeliveryType> getDeliveryType(byte[] deliveryContent) {
    return getDeliveryType(deliveryContent, StandardCharsets.UTF_8);
  }
}
