/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl;

import ch.ge.ve.model.convert.api.DuplicateElectionIdentifierException;
import ch.ge.ve.model.convert.api.MissingElectionIdentifierException;
import ch.ge.ve.model.convert.api.VoterChoiceConverter;
import ch.ge.ve.model.convert.impl.ech0157.ECH0157Parser;
import ch.ge.ve.model.convert.impl.ech0157.model.ECH0157ElectionInformation;
import ch.ge.ve.model.convert.impl.ech0159.ECH0159Parser;
import ch.ge.ve.model.convert.impl.ech0159.model.ECH0159Ballot;
import ch.ge.ve.model.convert.impl.ech0159.model.ECH0159VoteInformation;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import ch.ge.ve.model.convert.model.VoterChoice;
import ch.ge.ve.model.convert.model.VoterElectionChoice;
import ch.ge.ve.model.convert.model.VoterVotationChoice;
import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.ElectionSet;
import ch.ge.ve.protocol.model.PrintingAuthority;
import com.google.common.io.ByteStreams;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Default implementation class for {@link VoterChoiceConverter}.
 */
public class DefaultVoterChoiceConverter implements VoterChoiceConverter {
  private static final String  BLANK_BALLOT_ID                    = "BLANK_BALLOT";
  private static final String  EMPTY_LIST_ID                      = "EMPTY_LIST";
  private static final Pattern EMPTY_CANDIDATE_ID                 = Pattern.compile("EMPTY\\.\\d+");
  private static final Pattern ELECTORAL_ROLL_ELECTION_ID_PATTERN = Pattern.compile("(.+?)\\.LISTS$");

  private final ECH0159Parser ech0159Parser;
  private final ECH0157Parser ech0157Parser;

  public DefaultVoterChoiceConverter() {
    this.ech0159Parser = new ECH0159Parser();
    this.ech0157Parser = new ECH0157Parser();
  }


  private byte[] getContent(InputStream in) {
    try {
      return ByteStreams.toByteArray(in);
    } catch (IOException e) {
      throw new IllegalArgumentException("Cannot retrieve the content of the delivery stream", e);
    }
  }

  @Override
  public List<VoterChoice> convertToVoterChoiceList(ElectionSet<? extends PrintingAuthority> electionSet,
                                                    InputStream... deliveryStreams) {
    List<VoterChoice> result = new ArrayList<>();
    List<ECH0159VoteInformation> voteInformationList = new ArrayList<>();
    List<ECH0157ElectionInformation> electionInformationList = new ArrayList<>();

    Arrays.stream(deliveryStreams)
          .map(this::getContent)
          .forEach(content -> {
            DeliveryType type =
                DeliveryType.getDeliveryType(content)
                            .orElseThrow(() -> new IllegalArgumentException("Invalid delivery stream"));


            if (DeliveryType.ECH_0157.equals(type)) {
              electionInformationList.addAll(ech0157Parser.retrieveElectionInformation(content));
            } else if (DeliveryType.ECH_0159.equals(type)) {
              voteInformationList.addAll(ech0159Parser.retrieveVoteInformation(content));
            } else {
              throw new IllegalArgumentException(String.format("Unknown delivery type [%s]", type));
            }
          });

    List<Election> elections = electionSet.getElections();
    int currentCandidateIndex = 0;

    for (Election currentElection : elections) {
      int nbrOfCandidates = currentElection.getNumberOfCandidates();

      List<Candidate> currentCandidates = electionSet.getCandidates()
                                                     .subList(currentCandidateIndex,
                                                              currentCandidateIndex + nbrOfCandidates);

      result.addAll(mapElectionToVoteChoices(currentElection,
                                             currentCandidates,
                                             voteInformationList,
                                             electionInformationList));

      currentCandidateIndex += currentElection.getNumberOfCandidates();
    }

    return result;
  }

  private List<VoterChoice> mapElectionToVoteChoices(Election election,
                                                     List<Candidate> candidates,
                                                     List<ECH0159VoteInformation> voteInformationList,
                                                     List<ECH0157ElectionInformation> electionInformationList) {

    Optional<ECH0159VoteInformation> voteInformation =
        findVoteInformationByElectionIdentifier(voteInformationList, election.getIdentifier());

    Optional<ECH0157ElectionInformation> electionInformation =
        findElectionInformationByElectionIdentifier(electionInformationList,
                                                    toECH0157Identifier(election.getIdentifier()));

    if (voteInformation.isPresent() && electionInformation.isPresent()) {
      throw new DuplicateElectionIdentifierException(election.getIdentifier());
    }

    if (voteInformation.isPresent()) {
      return createVoterChoices(election, candidates, voteInformation.get());
    } else if (electionInformation.isPresent()) {
      return createVoterChoices(election, candidates, electionInformation.get());
    } else {
      throw new MissingElectionIdentifierException(election.getIdentifier());
    }

  }

  private Optional<ECH0159VoteInformation> findVoteInformationByElectionIdentifier(
      List<ECH0159VoteInformation> voteInformationList, String electionIdentifier) {

    return voteInformationList
        .stream()
        .filter(voteInformation -> voteInformation.hasQuestionIdentifier(electionIdentifier))
        .findFirst();
  }

  private Optional<ECH0157ElectionInformation> findElectionInformationByElectionIdentifier(
      List<ECH0157ElectionInformation> electionInformationList, String electionIdentifier) {
    return electionInformationList
        .stream()
        .filter(electionInformation -> electionIdentifier.equals(electionInformation.getElectionIdentification()))
        .findFirst();
  }

  private String toECH0157Identifier(String electionIdentifier) {
    return electionIdentifier.replace(".CANDIDATES", "").replace(".LISTS", "");
  }

  private List<VoterChoice> createVoterChoices(Election election,
                                               List<Candidate> candidates,
                                               ECH0159VoteInformation voteInformation) {
    ECH0159Ballot ech0159Ballot = findBallotByElectionIdentifier(voteInformation, election.getIdentifier());
    AnswerTypeEnum answerType = AnswerTypeEnum.fromInt(
        ech0159Ballot.getAnswerTypeCodeByQuestionIdentifier(election.getIdentifier())
    );

    return candidates.stream()
                     .map(candidate -> new VoterVotationChoice(election,
                                                               voteInformation.getVoteIdentification(),
                                                               toCandidateId(election, candidate),
                                                               ech0159Ballot.getBallotIdentification(),
                                                               answerType,
                                                               ech0159Ballot.isStandardBallot(),
                                                               ech0159Ballot.isVariantBallot())
                     ).collect(Collectors.toList());
  }

  private ECH0159Ballot findBallotByElectionIdentifier(ECH0159VoteInformation voteInformation,
                                                       String electionIdentifier) {

    return voteInformation.getBallots()
                          .stream()
                          .filter(ballot -> ballot.hasQuestionIdentifier(electionIdentifier))
                          .findFirst()
                          .orElseThrow(() -> new MissingElectionIdentifierException(electionIdentifier));

  }

  private String toCandidateId(Election election, Candidate candidate) {
    return candidate.getCandidateDescription().replace(election.getIdentifier() + ":", "");
  }

  private List<VoterChoice> createVoterChoices(Election election,
                                               List<Candidate> candidates,
                                               ECH0157ElectionInformation electionInformation) {

    return candidates.stream()
                     .map(candidate -> {
                       String candidateId = toCandidateId(election, candidate);
                       boolean isElectoralRollElection = isElectoralRollElection(election);
                       String electoralRollId = isElectoralRollElection ? null :
                           electionInformation.findElectionRollByCandidateId(candidateId).orElse(null);

                       return new VoterElectionChoice(election,
                                                      electionInformation.getElectionIdentification(),
                                                      candidateId,
                                                      electoralRollId,
                                                      electionInformation.isMajorityElection(),
                                                      electionInformation.isProportionalElection(),
                                                      isElectoralRollElection,
                                                      EMPTY_CANDIDATE_ID.matcher(candidateId).matches(),
                                                      EMPTY_LIST_ID.matches(candidateId),
                                                      BLANK_BALLOT_ID.matches(candidateId));
                     }).collect(Collectors.toList());
  }

  private boolean isElectoralRollElection(Election election) {
    return ELECTORAL_ROLL_ELECTION_ID_PATTERN.matcher(election.getIdentifier()).matches();
  }
}
