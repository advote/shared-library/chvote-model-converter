/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl.ech0159.model;

import ch.ge.ve.interfaces.ech.eCH0155.v4.AnswerInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType;
import java.math.BigInteger;
import java.util.stream.Stream;

/**
 * A {@link BallotType} decorator.
 */
public class ECH0159Ballot {
  private final BallotType ballot;

  /**
   * Create a new eCH-0159 ballot type decorator.
   *
   * @param ballot the {@link BallotType} to decorate.
   */
  public ECH0159Ballot(BallotType ballot) {
    this.ballot = ballot;
  }

  /**
   * Whether this is a standard ballot.
   *
   * @return true if its a standard ballot, false otherwise.
   */
  public boolean isStandardBallot() {
    return ballot.getStandardBallot() != null;
  }

  /**
   * Whether this is a variant ballot.
   *
   * @return true if its a variant ballot, false otherwise.
   */
  public boolean isVariantBallot() {
    return ballot.getVariantBallot() != null;
  }

  /**
   * Whether this ballot contains a question with the given identifier.
   *
   * @param identifier the question identifier.
   *
   * @return true if the ballot contains a question with the given identifier, false otherwise.
   */
  public boolean hasQuestionIdentifier(String identifier) {
    return streamQuestionIdentifiers().anyMatch(identifier::equals);
  }

  /**
   * Find the answer type code of the first question that matches the given identifier.
   *
   * @param identifier the question identifier.
   *
   * @return the answer type code.
   */
  public Integer getAnswerTypeCodeByQuestionIdentifier(String identifier) {
    if (isStandardBallot()) {
      BallotType.StandardBallot standardBallot = ballot.getStandardBallot();
      boolean matchesIdentifier = (standardBallot.getQuestionIdentification().equals(identifier));

      return matchesIdentifier ? standardBallot.getAnswerInformation().getAnswerType().intValue() : null;
    } else {
      return Stream.concat(ballot.getVariantBallot()
                                 .getQuestionInformation()
                                 .stream()
                                 .filter(question -> question.getQuestionIdentification().equals(identifier))
                                 .map(QuestionInformationType::getAnswerInformation),
                           ballot.getVariantBallot()
                                 .getTieBreakInformation()
                                 .stream()
                                 .filter(tieBreak -> tieBreak.getQuestionIdentification().equals(identifier))
                                 .map(TieBreakInformationType::getAnswerInformation))
                   .map(AnswerInformationType::getAnswerType)
                   .map(BigInteger::intValue).findFirst().orElse(null);
    }
  }

  /**
   * Get the identification string of the decorated. Same as calling: {@link BallotType#getBallotIdentification()}.
   *
   * @return the ballot identification string.
   */
  public String getBallotIdentification() {
    return ballot.getBallotIdentification();
  }

  private Stream<String> streamQuestionIdentifiers() {
    if (isStandardBallot()) {
      return Stream.of(ballot.getStandardBallot().getQuestionIdentification());
    } else if (isVariantBallot()) {
      return Stream.concat(ballot.getVariantBallot()
                                 .getQuestionInformation()
                                 .stream()
                                 .map(QuestionInformationType::getQuestionIdentification),
                           ballot.getVariantBallot()
                                 .getTieBreakInformation()
                                 .stream()
                                 .map(TieBreakInformationType::getQuestionIdentification));
    } else {
      throw new IllegalStateException(
          String.format("Neither standard ballot nor variant ballot defined for ballot with id [%s]",
                        ballot.getBallotIdentification()));
    }
  }

}
