/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl;

import ch.ge.ve.interfaces.ech.eCH0155.v4.CandidateType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ElectionType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ListType;
import ch.ge.ve.interfaces.ech.eCH0157.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0157.v4.EventInitialDelivery;
import ch.ge.ve.model.convert.api.ElectionConverter;
import ch.ge.ve.model.convert.model.ElectionConfiguration;
import ch.ge.ve.model.convert.model.ElectionDefinition;
import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import ch.ge.ve.protocol.model.Election;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation class for {@link ElectionConverter}.
 */
public class DefaultElectionConverter implements ElectionConverter {
  private static final Logger  log                         = LoggerFactory.getLogger(DefaultVoterConverter.class);
  private static final Integer PROPORTIONAL_ELECTION_TYPE  = 1;
  private static final Integer MAJORITY_ELECTION_TYPE      = 2;
  private static final String  BLANK_BALLOT                = "BLANK_BALLOT";
  private static final String  EMPTY_LIST                  = "EMPTY_LIST";
  private static final String  CANDIDATES_TEMPLATE         = "%s.CANDIDATES";
  private static final String  LISTS_TEMPLATE              = "%s.LISTS";
  private static final String  EMPTY_CANDIDATE_ID_TEMPLATE = "EMPTY.%d";

  private final JAXBContext jaxbContext;

  private final ConcurrentMap<String, DomainOfInfluence> doiByName = new ConcurrentHashMap<>();

  public DefaultElectionConverter() {
    try {
      this.jaxbContext = JAXBContext.newInstance(Delivery.class);
    } catch (JAXBException e) {
      log.error("Failed to instantiate DefaultElectionConverter", e);
      throw new IllegalStateException("Failed to instantiate JAXBContext for DefaultElectionConverter", e);
    }
  }

  @Override
  public List<ElectionDefinition> convertToElectionDefinitionList(InputStream... electionDeliveryStreams) {
    return convertToElectionDefinitionList(Collections.emptyList(), electionDeliveryStreams);
  }

  @Override
  public List<ElectionDefinition> convertToElectionDefinitionList(List<ElectionConfiguration> electionConfigurations,
                                                                  InputStream... electionDeliveryStreams) {
    Map<String, ElectionConfiguration> configuration =
        electionConfigurations.stream()
                              .collect(Collectors.toMap(
                                  ElectionConfiguration::getBallotIdentifier,
                                  Function.identity()));

    return Arrays.stream(electionDeliveryStreams)
                 .flatMap(in -> this.unmarshallAndConvert(configuration, in))
                 .collect(Collectors.toList());
  }


  private Stream<ElectionDefinition> unmarshallAndConvert(Map<String, ElectionConfiguration> configuration,
                                                          InputStream electionDeliveryStream) {
    try {
      XMLEventReader eventReader = XMLInputFactory.newFactory().createXMLEventReader(electionDeliveryStream);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      JAXBElement<Delivery> element = unmarshaller.unmarshal(eventReader, Delivery.class);
      return convertDelivery(configuration, element.getValue());
    } catch (XMLStreamException | JAXBException e) {
      log.error("Failed to parse input stream", e);
      throw new IllegalArgumentException("Invalid eCH-0157 stream", e);
    }
  }

  private Stream<ElectionDefinition> convertDelivery(Map<String, ElectionConfiguration> configuration,
                                                     Delivery delivery) {
    return delivery.getInitialDelivery().getElectionGroupBallot().stream()
                   .flatMap(electionGroupBallot -> this.convertElectionGroupBallot(configuration, electionGroupBallot));
  }

  private Stream<ElectionDefinition> convertElectionGroupBallot(
      Map<String, ElectionConfiguration> configuration,
      EventInitialDelivery.ElectionGroupBallot electionGroupBallot) {

    String domainOfInfluenceName = electionGroupBallot.getDomainOfInfluenceIdentification();
    DomainOfInfluence domainOfInfluence =
        doiByName.computeIfAbsent(domainOfInfluenceName, DomainOfInfluence::new);

    return electionGroupBallot.getElectionInformation().stream()
                              .flatMap(electionInformation -> buildElection(configuration,
                                                                            electionInformation,
                                                                            domainOfInfluence));
  }

  private Stream<ElectionDefinition> buildElection(
      Map<String, ElectionConfiguration> configuration,
      EventInitialDelivery.ElectionGroupBallot.ElectionInformation electionInformation,
      DomainOfInfluence domainOfInfluence) {

    ElectionType electionType = electionInformation.getElection();
    ElectionConfiguration electionConfiguration = configuration.get(electionType.getElectionIdentification());
    boolean hasVoidElectoralRollWithNoCandidates = electionConfiguration != null &&
                                                   electionConfiguration.isHasVoidElectoralRollWithNoCandidates() &&
                                                   isProportionalElection(electionType);
    List<Candidate> candidates = buildCandidates(electionType,
                                                 hasVoidElectoralRollWithNoCandidates,
                                                 electionInformation.getCandidate());

    if (isProportionalElection(electionType)) {
      return buildProportionalElection(candidates,
                                       hasVoidElectoralRollWithNoCandidates,
                                       electionInformation,
                                       domainOfInfluence);
    } else if (isMajorityElection(electionType)) {
      return buildMajorityElection(candidates, electionInformation, domainOfInfluence);
    } else {
      throw new IllegalArgumentException(String.format("Unknown election type [%s]", electionType.getTypeOfElection()));
    }
  }

  private List<Candidate> buildCandidates(ElectionType electionType,
                                          boolean hasVoidElectoralRollWithNoCandidates,
                                          List<CandidateType> candidateTypes) {

    List<Candidate> candidates = candidateTypes.stream()
                                               .map(CandidateType::getCandidateIdentification)
                                               .map(Candidate::new).collect(Collectors.toList());

    int nbrOfMandates = electionType.getNumberOfMandates().intValue();
    nbrOfMandates = hasVoidElectoralRollWithNoCandidates ? nbrOfMandates - 1 : nbrOfMandates;

    for (int i = 0; i < nbrOfMandates; i++) {
      candidates.add(new Candidate(String.format(EMPTY_CANDIDATE_ID_TEMPLATE, i)));
    }

    return candidates;

  }

  private Stream<ElectionDefinition> buildProportionalElection(
      List<Candidate> candidates,
      boolean hasVoidElectoralRollWithNoCandidates,
      EventInitialDelivery.ElectionGroupBallot.ElectionInformation electionInformation,
      DomainOfInfluence domainOfInfluence) {

    ElectionType electionType = electionInformation.getElection();
    List<Candidate> rolls = buildElectionRollsAsCandidates(hasVoidElectoralRollWithNoCandidates,
                                                           electionInformation.getList());
    String candidateId = String.format(CANDIDATES_TEMPLATE, electionType.getElectionIdentification());
    String electionId = String.format(LISTS_TEMPLATE, electionType.getElectionIdentification());

    Election candidatesElection = new Election(candidateId,
                                               candidates.size(),
                                               electionType.getNumberOfMandates().intValue(),
                                               domainOfInfluence);
    Election rollsElection = new Election(electionId,
                                          rolls.size(),
                                          1,
                                          domainOfInfluence);

    return Stream.of(new ElectionDefinition(candidatesElection, candidates),
                     new ElectionDefinition(rollsElection, rolls));
  }

  private Stream<ElectionDefinition> buildMajorityElection(
      List<Candidate> candidates,
      EventInitialDelivery.ElectionGroupBallot.ElectionInformation electionInformation,
      DomainOfInfluence domainOfInfluence) {
    ElectionType electionType = electionInformation.getElection();

    Election election = new Election(electionType.getElectionIdentification(),
                                     candidates.size(),
                                     electionType.getNumberOfMandates().intValue(),
                                     domainOfInfluence);

    return Stream.of(new ElectionDefinition(election, candidates));
  }

  private List<Candidate> buildElectionRollsAsCandidates(boolean hasVoidElectoralRollWithNoCandidates,
                                                         List<ListType> listTypes) {

    List<Candidate> candidates = listTypes.stream()
                                          .map(ListType::getListIdentification)
                                          .map(Candidate::new)
                                          .collect(Collectors.toList());

    if (hasVoidElectoralRollWithNoCandidates) {
      candidates.add(new Candidate(BLANK_BALLOT));
    }

    candidates.add(new Candidate(EMPTY_LIST));

    return candidates;
  }

  private boolean isProportionalElection(ElectionType electionType) {
    return PROPORTIONAL_ELECTION_TYPE.equals(electionType.getTypeOfElection().intValue());
  }

  private boolean isMajorityElection(ElectionType electionType) {
    return MAJORITY_ELECTION_TYPE.equals(electionType.getTypeOfElection().intValue());
  }
}
