/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl.ech0157;

import ch.ge.ve.interfaces.ech.eCH0157.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0157.v4.EventInitialDelivery.ElectionGroupBallot;
import ch.ge.ve.model.convert.impl.ech0157.model.ECH0157ElectionInformation;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An object to parse an operation reference eCH-0157 XML file int a list of {@link ECH0157ElectionInformation}.
 */
public class ECH0157Parser {
  private static final Logger log = LoggerFactory.getLogger(ECH0157Parser.class);

  private final JAXBContext     jaxbContext;
  private final XMLInputFactory xmlInputFactory;

  /**
   * Create a new eCH-0157 parser.
   */
  public ECH0157Parser() {
    try {
      this.jaxbContext = JAXBContext.newInstance(Delivery.class);
      this.xmlInputFactory = XMLInputFactory.newFactory();
    } catch (Exception e) {
      log.error("Failed to instantiate ECH0157Parser", e);
      throw new IllegalStateException("Failed to instantiate JAXB elements for ECH0157Parser", e);
    }
  }

  private List<ECH0157ElectionInformation> retrieveElectionInformation(InputStream electionDeliveryStreams) {
    try {
      XMLEventReader reader = xmlInputFactory.createXMLEventReader(electionDeliveryStreams);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      Delivery delivery = unmarshaller.unmarshal(reader, Delivery.class).getValue();
      return delivery.getInitialDelivery().getElectionGroupBallot()
                     .stream().map(ElectionGroupBallot::getElectionInformation)
                     .flatMap(List::stream).map(ECH0157ElectionInformation::new)
                     .collect(Collectors.toList());
    } catch (XMLStreamException | JAXBException e) {
      throw new IllegalArgumentException("Invalid eCH-0157 stream", e);
    }
  }

  /**
   * Retrieve a list of election information for the given list of ECH0157 delivery stream.
   *
   * @param electionDeliveryStreams the locations of the ECH0157 delivery stream.
   *
   * @return the list of {@link ECH0157ElectionInformation} as they appear in the files.
   */
  public List<ECH0157ElectionInformation> retrieveElectionInformation(InputStream... electionDeliveryStreams) {
    return Arrays.stream(electionDeliveryStreams)
                 .map(this::retrieveElectionInformation)
                 .flatMap(List::stream)
                 .collect(Collectors.toList());
  }

  /**
   * Works as {@link #retrieveElectionInformation(InputStream...)} but the input is the content of a single delivery
   * as a byte array.
   *
   * @see #retrieveElectionInformation(InputStream...)
   */
  public List<ECH0157ElectionInformation> retrieveElectionInformation(byte[] electionDelivery) {
    try (InputStream in = new ByteArrayInputStream(electionDelivery)) {
      return retrieveElectionInformation(in);
    } catch (IOException e) {
      throw new IllegalArgumentException("Invalid eCH-0159 stream", e);
    }
  }
}
