/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl

import ch.ge.ve.model.convert.api.PrinterMappingMismatchException
import ch.ge.ve.model.convert.api.StatsCounter
import ch.ge.ve.model.convert.model.PartialLocalDate
import ch.ge.ve.model.convert.model.VoterDetails
import java.time.Month
import java.util.stream.Collectors
import spock.lang.Specification

class DefaultVoterConverterTest extends Specification {
  VoterIdGenerator voterIdGenerator
  DefaultVoterConverter voterConverter

  void setup() {
    voterIdGenerator = new VoterIdGenerator()
    voterConverter = new DefaultVoterConverter(voterIdGenerator)
  }

  def "it should successfully convert a simple xml file for SR"() {
    given: "an eCH-0045 file"
    def resource = DefaultVoterConverterTest.classLoader.getResourceAsStream("eCH-0045-1-voter.xml")
    def deliveryId = "test-manufacturer-product-2017-10-20T15:21:57.481-messageID1"

    when:
    def voterList = voterConverter.convertToVoterList(
            [6621: "printingAuth1"],
            ["GE": "printingAuth2"],
            resource).collect(Collectors.toList())

    then:
    voterList != null
    voterList.size() == 1
    def voter = voterList.get(0)
    voter.id == 0
    voter.registerPersonId == voterIdGenerator.generateId(deliveryId, "id", "1")
    voter.printingAuthorityName == "printingAuth1"
    def countingCircle = voter.countingCircle
    countingCircle.id == 0
    countingCircle.businessId == "COUNTING_CIRCLE_ID_0"
    countingCircle.name == "COUNTING_CIRCLE_NAME_0"
    voter.allowedDomainsOfInfluence.size() == 1
    def doi = voter.allowedDomainsOfInfluence.get(0)
    doi.identifier == "1"
  }

  def "it should successfully convert a simple xml file for SE bound to a canton"() {
    given: "an eCH-0045 file"
    def resource = DefaultVoterConverterTest.classLoader.getResourceAsStream("eCH-0045-1-SE-canton.xml")
    def deliveryId = "test-manufacturer-product-2017-10-21T15:21:57.481-messageID2"

    when:
    def voterList = voterConverter.convertToVoterList(
            [:],
            ["GE": "printingAuth1"],
            resource).collect(Collectors.toList())

    then:
    voterList != null
    voterList.size() == 1
    def voter = voterList.get(0)
    voter.id == 0
    voter.registerPersonId == voterIdGenerator.generateId(deliveryId, "id", "0")
    voter.printingAuthorityName == "printingAuth1"
    def countingCircle = voter.countingCircle
    countingCircle.id == 0
    countingCircle.businessId == "413"
    countingCircle.name == "Cernier"
    voter.allowedDomainsOfInfluence.size() == 1
    def doi = voter.allowedDomainsOfInfluence.get(0)
    doi.identifier == "1"
  }

  def "it should successfully convert two xml files"() {
    given: "two eCH-0045 files"
    def resource_SR = DefaultVoterConverterTest.classLoader.getResourceAsStream("eCH-0045-1-voter.xml")
    def deliveryId_SR = "test-manufacturer-product-2017-10-20T15:21:57.481-messageID1"
    def resource_SE = DefaultVoterConverterTest.classLoader.getResourceAsStream("eCH-0045-1-SE-canton.xml")
    def deliveryId_SE = "test-manufacturer-product-2017-10-21T15:21:57.481-messageID2"

    when:
    def voterList = voterConverter.convertToVoterList(
            [6621: "printingAuth1"],
            ["GE": "printingAuth1"],
            resource_SR, resource_SE)
            .collect(Collectors.toList())

    then:
    voterList != null
    voterList.size() == 2

    def voter1Optional = voterList.stream()
            .filter { voter -> voter.registerPersonId == voterIdGenerator.generateId(deliveryId_SR, "id", "1") }
            .findFirst()
    voter1Optional.isPresent()
    def voter1 = voter1Optional.get()
    voter1.dateOfBirth.type == PartialLocalDate.Type.YEAR
    voter1.dateOfBirth.year.value == 1950
    voter1.dateOfBirth.monthOfYear.empty()
    voter1.dateOfBirth.dayOfMonth.empty()
    voter1.printingAuthorityName == "printingAuth1"
    def countingCircle1 = voter1.countingCircle
    countingCircle1.businessId == "COUNTING_CIRCLE_ID_0"
    countingCircle1.name == "COUNTING_CIRCLE_NAME_0"
    voter1.allowedDomainsOfInfluence.size() == 1
    def doi1 = voter1.allowedDomainsOfInfluence.get(0)
    doi1.identifier == "1"

    def voter2Optional = voterList.stream()
            .filter { voter -> voter.registerPersonId == voterIdGenerator.generateId(deliveryId_SE, "id", "0") }
            .findFirst()
    voter2Optional.isPresent()
    def voter2 = voter2Optional.get()
    voter2.dateOfBirth.type == PartialLocalDate.Type.YEAR_MONTH
    voter2.dateOfBirth.year.value == 2000
    voter2.dateOfBirth.monthOfYear == Optional.of(Month.SEPTEMBER)
    voter2.dateOfBirth.dayOfMonth.empty()
    voter2.printingAuthorityName == "printingAuth1"
    def countingCircle2 = voter2.countingCircle
    countingCircle2.businessId == "413"
    countingCircle2.name == "Cernier"
    voter2.allowedDomainsOfInfluence.size() == 1
    def doi2 = voter2.allowedDomainsOfInfluence.get(0)
    doi2.identifier == "1"
  }

  def "it should fail to convert if the counting circle information is missing"() {
    given: "an eCH-0045 file"
    def resource = DefaultVoterConverterTest.classLoader.getResourceAsStream("eCH-0045-bad.xml")

    when:
    voterConverter.convertToVoterList(
            [6621: "printingAuth1"],
            ["GENEVA": "printingAuth2"],
            resource).collect(Collectors.toList())

    then:
    thrown IllegalArgumentException
  }

  def "it should be possible to count the number of voters by printer"() {
    given: "an eCH-0045 file and an output streams for the GENEVA municipality printer"
    def r1 = getRegistryStream("register-eCH0045-0-swiss-13-swissAbroad-0-foreigner.xml")
    def r2 = getRegistryStream("register-eCH0045-7-swiss-0-swissAbroad-12-foreigner.xml")
    def r3 = getRegistryStream("register-eCH0045-10-swiss-0-swissAbroad-5-foreigner.xml")
    def printingAuthorityByMunicipality = [
            2110: "printingAuth2",
            2120: "printingAuth1"
    ]
    def printingAuthoritiesByCanton = [
            "BE": "printingAuth1",
            "TG": "printingAuth1",
            "SH": "printingAuth1",
            "VD": "printingAuth1"
    ]
    def counter = new StatsCounter({ VoterDetails voter -> voter.printingAuthorityName })

    when:
    def nbVoters = voterConverter.convertToVoterList(
            printingAuthorityByMunicipality, printingAuthoritiesByCanton, [r1, r2, r3] as InputStream[])
            .peek({ VoterDetails voter -> counter.append(voter) })
            .count()

    then:
    nbVoters == 47
    counter.getTotal() == nbVoters
    counter.getCountFor("printingAuth1") == 44
    counter.getCountFor("printingAuth2") == 3
  }

  def "it should successfully remap a simple xml file"() {
    given: "an eCH-0045 file and an output streams for the GENEVA municipality printer"
    def echvoterInputStream = getRegistryStream("eCH-0045-1-voter.xml")
    def deliveryId = "test-manufacturer-product-2017-10-20T15:21:57.481-messageID1"
    def resultPrinter1 = new ByteArrayOutputStream()
    def receivers = [
            "printingAuth1": resultPrinter1
    ]
    def printingAuthorityByMunicipality = [6621: "printingAuth1"]
    def printingAuthoritiesByCanton = [:]
    def nbrOfVotersByPrinter = [printingAuth1: 1L]

    when:

    voterConverter.remapByPrinter(
            receivers,
            printingAuthorityByMunicipality,
            printingAuthoritiesByCanton,
            nbrOfVotersByPrinter,
            [echvoterInputStream] as InputStream[]
    )

    then:
    def voterDelivery = new XmlSlurper().parseText(resultPrinter1.toString())
    voterDelivery.deliveryHeader.senderId == "admin.chvote.ch"
    voterDelivery.voterList.numberOfVoters == 1
    def person = voterDelivery.voterList.voter[0].person.swiss.swissDomesticPerson
    person.personIdentification.localPersonId.personId == voterIdGenerator.generateId(deliveryId, "id", "1")
  }

  def "it should fail if the printer does not have a matching output stream"() {
    given: "an eCH-0045 file and an output streams for the GENEVA municipality printer"
    def echvoterInputStream = getRegistryStream("eCH-0045-1-voter.xml")
    def resultPrinter1 = new ByteArrayOutputStream()
    def receivers = [
            "printingAuth2": resultPrinter1
    ]
    def printingAuthorityByMunicipality = [6621: "printingAuth1"]
    def printingAuthoritiesByCanton = [:]
    def nbrOfVotersByPrinter = [printingAuth1: 1L]

    when:
    voterConverter.remapByPrinter(
            receivers,
            printingAuthorityByMunicipality,
            printingAuthoritiesByCanton,
            nbrOfVotersByPrinter,
            [echvoterInputStream] as InputStream[]
    )

    then:
    thrown PrinterMappingMismatchException
  }

  def "it should fail if the printer does not cover the municipality of the voter in the registry"() {
    given: "an eCH-0045 file and an output streams for the GENEVA municipality printer"
    def echvoterInputStream = getRegistryStream("eCH-0045-1-voter.xml")
    def resultPrinter1 = new ByteArrayOutputStream()
    def receivers = [
            "printingAuth1": resultPrinter1
    ]
    def printingAuthoritiesByCanton = [:]
    def nbrOfVotersByPrinter = [printingAuth1: 1L]

    when:
    voterConverter.remapByPrinter(
            receivers,
            [6601: "printingAuth1"],
            printingAuthoritiesByCanton,
            nbrOfVotersByPrinter,
            [echvoterInputStream] as InputStream[]
    )

    then:
    thrown PrinterMappingMismatchException
  }

  def "it should successfully remap several xml files"() {
    given: "3 eCH-0045 file and an output streams for the GENEVA municipality printer"
    def registry_1 = getRegistryStream("register-eCH0045-0-swiss-13-swissAbroad-0-foreigner.xml")
    def registry_2 = getRegistryStream("register-eCH0045-7-swiss-0-swissAbroad-12-foreigner.xml")
    def registry_3 = getRegistryStream("register-eCH0045-10-swiss-0-swissAbroad-5-foreigner.xml")

    def resultPrinter1 = new ByteArrayOutputStream()
    def resultPrinter2 = new ByteArrayOutputStream()
    def receivers = [
            "printingAuth1": resultPrinter1,
            "printingAuth2": resultPrinter2
    ]
    def printingAuthorityByMunicipality = [
            2110: "printingAuth2",
            2120: "printingAuth1"
    ]
    def printingAuthoritiesByCanton = [
            "BE": "printingAuth1",
            "TG": "printingAuth1",
            "SH": "printingAuth1",
            "VD": "printingAuth1"
    ]
    def nbrOfVotersByPrinter = [printingAuth2: 3L, printingAuth1: 44L]

    when:
    voterConverter.remapByPrinter(
            receivers,
            printingAuthorityByMunicipality,
            printingAuthoritiesByCanton,
            nbrOfVotersByPrinter,
            [registry_1, registry_2, registry_3] as InputStream[]
    )

    then: "Printer 1 should have almost all voters"
    def voterDelivery1 = new XmlSlurper().parseText(resultPrinter1.toString())
    voterDelivery1.deliveryHeader.senderId == "admin.chvote.ch"
    voterDelivery1.voterList.numberOfVoters == 44
    voterDelivery1.voterList.voter[0].person.swissAbroad.swissAbroadPerson.personIdentification.officialName == "Ried"
    voterDelivery1.voterList.voter[12].person.swiss.swissDomesticPerson.personIdentification.officialName == "Maker"
    voterDelivery1.voterList.voter[19].person.foreigner.foreignerPerson.personIdentification.officialName == "Mcgown"
    voterDelivery1.voterList.voter[30].person.swiss.swissDomesticPerson.personIdentification.officialName == "Greenwaldt"
    voterDelivery1.voterList.voter[39].person.foreigner.foreignerPerson.personIdentification.officialName == "Schappell"

    and: "Printer 2 should have the 3 voters on Winterthour municipality"
    def voterDelivery2 = new XmlSlurper().parseText(resultPrinter2.toString())
    voterDelivery2.deliveryHeader.senderId == "admin.chvote.ch"
    voterDelivery2.voterList.numberOfVoters == 3
    voterDelivery2.voterList.voter[0].person.swissAbroad.swissAbroadPerson.personIdentification.officialName == "Develice"
    voterDelivery2.voterList.voter[1].person.foreigner.foreignerPerson.personIdentification.officialName == "Aldrige"
    voterDelivery2.voterList.voter[2].person.swiss.swissDomesticPerson.personIdentification.officialName == "Nealis"
  }

  private static InputStream getRegistryStream(String registry) {
    DefaultVoterConverterTest.classLoader.getResourceAsStream(registry)
  }
}
