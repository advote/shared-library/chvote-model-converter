/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl

import java.util.stream.IntStream
import spock.lang.Specification

class VoterIdGeneratorTest extends Specification {
  private static final char[] ALPHABET = "0123456789abcdefghijklmnopqrstuvwxzyABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()

  static Random random
  static VoterIdGenerator generator

  void setupSpec() {
    random = new Random()
    generator = new VoterIdGenerator()
  }

  def "it should generate 36-characters long identifiers"() {
    expect:
    generator.generateId(deliveryId, registerId, personId).length() == 36

    where:
    deliveryId << randomStrings()
    registerId << randomStrings()
    personId << randomStrings()
  }

  def "it should generate identifiers deterministically"() {
    expect:
    generator.generateId(deliveryId, registerId, personId) == generator.generateId(deliveryId, registerId, personId)

    where:
    deliveryId << randomStrings()
    registerId << randomStrings()
    personId << randomStrings()
  }

  def "it should generate different identifiers for different inputs"() {
    expect:
    generator.generateId(deliveryId, registerId, personId) != generator.generateId(deliveryId2, registerId, personId)
    generator.generateId(deliveryId, registerId, personId) != generator.generateId(deliveryId, registerId2, personId)
    generator.generateId(deliveryId, registerId, personId) != generator.generateId(deliveryId, registerId, personId2)
    generator.generateId(deliveryId, registerId, personId) != generator.generateId(deliveryId2, registerId2, personId2)

    where:
    deliveryId << randomStrings(16)
    registerId << randomStrings(16)
    personId << randomStrings(16)
    deliveryId2 << randomStrings(8)
    registerId2 << randomStrings(8)
    personId2 << randomStrings(8)
  }

  static String[] randomStrings(int length = 32, int count = 1000) {
    IntStream.range(0, count).boxed()
            .map { randomString(length, ALPHABET) }
            .toArray()
  }

  static String randomString(int length, char[] chars) {
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      sb.append(chars[random.nextInt(chars.length)]);
    }
    return sb.toString();
  }
}
