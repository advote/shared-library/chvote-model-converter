/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl

import ch.ge.ve.protocol.model.Candidate
import ch.ge.ve.protocol.model.DomainOfInfluence
import spock.lang.Specification

class DefaultElectionConverterTest extends Specification {
  DefaultElectionConverter converter

  void setup() {
    converter = new DefaultElectionConverter()
  }

  def "it should successfully convert an eCH-0157 xml stream for a majority election"() {
    given: "a valid eCH-01597 xml stream"
    def resource = DefaultVoterConverterTest.classLoader.getResourceAsStream("eCH-0157-majority-election.xml")

    when: "converting the input stream"
    def electionDefinitions = converter.convertToElectionDefinitionList(resource)

    then: "the election definitions should not be null"
    electionDefinitions != null

    and: "the result should have the expected size"
    electionDefinitions.size() == 1

    and: "the election identification should be 2"
    electionDefinitions[0].election.identifier == "2"

    and: "the number of selections should be 7"
    electionDefinitions[0].election.numberOfSelections == 7

    and: "the declared number of candidates should be as expected (31) + the empty candidates (7)"
    electionDefinitions[0].election.numberOfCandidates == 38

    and: "the domain of influence should be the expected one"
    electionDefinitions[0].election.applicableDomainOfInfluence == new DomainOfInfluence("GE")

    and: "the number of candidates should be as expected"
    electionDefinitions[0].candidates.size() == 38

    and: "the 'candidates' should be the ones expected"
    electionDefinitions[0].candidates.containsAll([
            new Candidate("20003"),
            new Candidate("20004"),
            new Candidate("20005"),
            new Candidate("EMPTY.0"),
            new Candidate("EMPTY.1"),
            new Candidate("EMPTY.2"),
            new Candidate("EMPTY.3"),
            new Candidate("EMPTY.4"),
            new Candidate("EMPTY.5"),
            new Candidate("EMPTY.6")])
  }

  def "it should successfully convert an eCH-0157 xml stream for a proportional election"() {
    given: "a valid eCH-01597 xml stream"
    def resource = DefaultVoterConverterTest.classLoader.getResourceAsStream("eCH-0157-proportional-election.xml")

    when: "converting the input stream"
    def electionDefinitions = converter.convertToElectionDefinitionList(resource)

    then: "the election definitions should not be null"
    electionDefinitions != null

    and: "the result should one definition for the candidates and one for the electoral lists"
    electionDefinitions.size() == 2

    and: "the election identifier should be 1.CANDIDATES for the candidates election and 1.LISTS for the list election"
    electionDefinitions[0].election.identifier == "1.CANDIDATES"
    electionDefinitions[1].election.identifier == "1.LISTS"

    and: "the number of selections should be 10 for the candidates election"
    electionDefinitions[0].election.numberOfSelections == 10

    and: "the declared number of candidates should be as expected (31) + the empty candidates (10)"
    electionDefinitions[0].election.numberOfCandidates == 41

    and: "the number of selections should be 1 for the lists election"
    electionDefinitions[1].election.numberOfSelections == 1

    and: "the declared number of lists should be as expected (2) + the empty candidates (1)"
    electionDefinitions[1].election.numberOfCandidates == 3

    and: "the domain of influence should be the expected one"
    electionDefinitions[0].election.applicableDomainOfInfluence == new DomainOfInfluence("GE")
    electionDefinitions[1].election.applicableDomainOfInfluence == new DomainOfInfluence("GE")

    and: "the number of candidates should be as expected"
    electionDefinitions[0].candidates.size() == 41
    electionDefinitions[1].candidates.size() == 3

    and: "the 'candidates' should be the ones expected"
    electionDefinitions[0].candidates.containsAll([
            new Candidate("20003"),
            new Candidate("20004"),
            new Candidate("20005"),
            new Candidate("EMPTY.0"),
            new Candidate("EMPTY.1"),
            new Candidate("EMPTY.2"),
            new Candidate("EMPTY.3"),
            new Candidate("EMPTY.4"),
            new Candidate("EMPTY.5"),
            new Candidate("EMPTY.6"),
            new Candidate("EMPTY.7"),
            new Candidate("EMPTY.8"),
            new Candidate("EMPTY.9")])
    electionDefinitions[1].candidates.containsAll([
            new Candidate("101"),
            new Candidate("102"),
            new Candidate("EMPTY_LIST")])
  }
}
