/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl.ech0159

import spock.lang.Specification

class ECH0159ParserTest extends Specification {

  def "should parse an eCH-0159 with standard ballots"() {
    given:
    def operationReference = ECH0159ParserTest.classLoader.getResourceAsStream("eCH-0159-standard-ballot.xml")
    def parser = new ECH0159Parser()

    when:
    def voteInformation = parser.retrieveVoteInformation(operationReference)

    then:
    voteInformation != null
    voteInformation.size() == 6
    voteInformation.get(0).getVoteIdentification() == "205706VP-FED-CH"
    voteInformation.get(0).hasQuestionIdentifier("FED_CH_Q3")

    voteInformation.get(0).getBallots() != null
    voteInformation.get(0).getBallots().size() == 3
    voteInformation.get(0).getBallots().get(0).getBallotIdentification() == "FED_CH_Q1"
    voteInformation.get(0).getBallots().get(0).hasQuestionIdentifier("FED_CH_Q1")
    !voteInformation.get(0).getBallots().get(0).hasQuestionIdentifier("FED_CH_Q3")
    voteInformation.get(0).getBallots().get(0).getAnswerTypeCodeByQuestionIdentifier("FED_CH_Q1") == 2
    voteInformation.get(0).getBallots().get(0).getAnswerTypeCodeByQuestionIdentifier("FED_CH_Q3") == null

    !voteInformation.get(1).hasQuestionIdentifier("FED_CH_Q3")
  }

  def "should parse an eCH-0159 with variant ballots"() {
    given:
    def operationReference = ECH0159ParserTest.classLoader.getResourceAsStream("eCH-0159-variant-ballot.xml")
    def parser = new ECH0159Parser()

    when:
    def voteInformation = parser.retrieveVoteInformation(operationReference)

    then:
    voteInformation != null
    voteInformation.size() == 3
    voteInformation.get(0).getVoteIdentification() == "201X04VE-CAN-GE"
    voteInformation.get(0).hasQuestionIdentifier("CAN_GE_Q1.a")
    voteInformation.get(0).hasQuestionIdentifier("CAN_GE_Q1.b")
    voteInformation.get(0).hasQuestionIdentifier("CAN_GE_Q1.c")
    !voteInformation.get(0).hasQuestionIdentifier("FED_CH_Q1.a")

    voteInformation.get(0).getBallots() != null
    voteInformation.get(0).getBallots().size() == 1
    voteInformation.get(0).getBallots().get(0).hasQuestionIdentifier("CAN_GE_Q1.a")
    voteInformation.get(0).getBallots().get(0).hasQuestionIdentifier("CAN_GE_Q1.b")
    voteInformation.get(0).getBallots().get(0).hasQuestionIdentifier("CAN_GE_Q1.c")
    !voteInformation.get(0).getBallots().get(0).hasQuestionIdentifier("FED_CH_Q1.a")

    voteInformation.get(0).getBallots().get(0).getBallotIdentification() == "CAN_GE_Q1"
    voteInformation.get(0).getBallots().get(0).getAnswerTypeCodeByQuestionIdentifier("CAN_GE_Q1.a") == 2
    voteInformation.get(0).getBallots().get(0).getAnswerTypeCodeByQuestionIdentifier("CAN_GE_Q1.b") == 2
    voteInformation.get(0).getBallots().get(0).getAnswerTypeCodeByQuestionIdentifier("CAN_GE_Q1.c") == 5
    voteInformation.get(0).getBallots().get(0).getAnswerTypeCodeByQuestionIdentifier("FED_CH_Q1.a") == null

    !voteInformation.get(1).hasQuestionIdentifier("CAN_GE_Q1.a")
  }


}
