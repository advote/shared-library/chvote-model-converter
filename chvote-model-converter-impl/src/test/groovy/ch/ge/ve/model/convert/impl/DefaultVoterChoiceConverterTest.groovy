/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.model.convert.impl

import ch.ge.ve.model.convert.api.ElectionSetBuilder
import ch.ge.ve.model.convert.model.AnswerTypeEnum
import ch.ge.ve.model.convert.model.ElectionDefinition
import ch.ge.ve.model.convert.model.VoterElectionChoice
import ch.ge.ve.model.convert.model.VoterVotationChoice
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey
import spock.lang.Specification

class DefaultVoterChoiceConverterTest extends Specification {

  def "should retrieve the voter choices for an eCH-0159 with standard ballots"() {
    given:
    def operationReference = DefaultVoterChoiceConverterTest.classLoader.getResourceAsStream("eCH-0159-standard-ballot.xml")
    def electionDefinitions = createElectionDefinitionsForECH0159("eCH-0159-standard-ballot.xml")
    def parser = new DefaultVoterChoiceConverter()

    when:
    def voterChoices = parser.convertToVoterChoiceList(
            createElectionSet(electionDefinitions),
            [operationReference] as InputStream[]
    ) as List<VoterVotationChoice>

    then:
    voterChoices != null
    voterChoices.size() == 42

    then: "choice 0 is YES for question FED_CH_Q1"
    voterChoices.get(0).getElection().getIdentifier() == "FED_CH_Q1"
    voterChoices.get(0).getVoteId() == "205706VP-FED-CH"
    voterChoices.get(0).getCandidateId() == "YES"
    voterChoices.get(0).getBallotId() == "FED_CH_Q1"
    voterChoices.get(0).getAnswerRank() == 0
    voterChoices.get(0).getAnswerType() == AnswerTypeEnum.YES_NO_BLANK
    voterChoices.get(0).isStandardBallot()
    !voterChoices.get(0).isVariantBallot()

    then: "choice 1 is NO for question FED_CH_Q1"
    voterChoices.get(1).getElection().getIdentifier() == "FED_CH_Q1"
    voterChoices.get(1).getVoteId() == "205706VP-FED-CH"
    voterChoices.get(1).getCandidateId() == "NO"
    voterChoices.get(1).getBallotId() == "FED_CH_Q1"
    voterChoices.get(1).getAnswerRank() == 1
    voterChoices.get(1).getAnswerType() == AnswerTypeEnum.YES_NO_BLANK
    voterChoices.get(1).isStandardBallot()
    !voterChoices.get(1).isVariantBallot()

    then: "choice 2 is BLANK for question FED_CH_Q1"
    voterChoices.get(2).getElection().getIdentifier() == "FED_CH_Q1"
    voterChoices.get(2).getVoteId() == "205706VP-FED-CH"
    voterChoices.get(2).getCandidateId() == "BLANK"
    voterChoices.get(2).getBallotId() == "FED_CH_Q1"
    voterChoices.get(2).getAnswerRank() == 2
    voterChoices.get(2).getAnswerType() == AnswerTypeEnum.YES_NO_BLANK
    voterChoices.get(2).isStandardBallot()
    !voterChoices.get(2).isVariantBallot()
  }

  def "should retrieve the voter choices for an eCH-0157 with a majority election"() {
    given:
    def operationReference = DefaultVoterChoiceConverterTest.classLoader.getResourceAsStream("eCH-0157-majority-election.xml")
    def electionDefinitions = createElectionDefinitionsForECH0157("eCH-0157-majority-election.xml")

    def parser = new DefaultVoterChoiceConverter()

    when:
    def voterChoices = parser.convertToVoterChoiceList(
            createElectionSet(electionDefinitions),
            [operationReference] as InputStream[]
    ) as List<VoterElectionChoice>

    then:
    voterChoices != null
    voterChoices.size() == 38

    then: "choice 0 is 20003 for election 2"
    voterChoices.get(0).getElection().getIdentifier() == "2"
    voterChoices.get(0).getVoteId() == "2"
    voterChoices.get(0).getCandidateId() == "20003"
    !voterChoices.get(0).isProportionalElection()
    voterChoices.get(0).isMajorityElection()
    !voterChoices.get(0).isElectoralRoll()
    !voterChoices.get(0).isBlankBallot()
    !voterChoices.get(0).isEmptyCandidate()
    !voterChoices.get(0).isEmptyList()

    then: "choice 1 is 20004 for election 2"
    voterChoices.get(1).getElection().getIdentifier() == "2"
    voterChoices.get(1).getVoteId() == "2"
    voterChoices.get(1).getCandidateId() == "20004"
    !voterChoices.get(1).isProportionalElection()
    voterChoices.get(1).isMajorityElection()
    !voterChoices.get(1).isElectoralRoll()
    !voterChoices.get(1).isBlankBallot()
    !voterChoices.get(1).isEmptyCandidate()
    !voterChoices.get(1).isEmptyList()

    then: "choice 2 is 20005 for election 2"
    voterChoices.get(2).getElection().getIdentifier() == "2"
    voterChoices.get(12).getVoteId() == "2"
    voterChoices.get(2).getCandidateId() == "20005"
    !voterChoices.get(2).isProportionalElection()
    voterChoices.get(2).isMajorityElection()
    !voterChoices.get(2).isElectoralRoll()
    !voterChoices.get(2).isBlankBallot()
    !voterChoices.get(2).isEmptyCandidate()
    !voterChoices.get(2).isEmptyList()

    then: "choice 30 is EMPTY.0 for election 2"
    voterChoices.get(31).getElection().getIdentifier() == "2"
    voterChoices.get(31).getVoteId() == "2"
    voterChoices.get(31).getCandidateId() == "EMPTY.0"
    !voterChoices.get(31).isProportionalElection()
    voterChoices.get(31).isMajorityElection()
    !voterChoices.get(31).isElectoralRoll()
    !voterChoices.get(31).isBlankBallot()
    voterChoices.get(31).isEmptyCandidate()
    !voterChoices.get(31).isEmptyList()
  }

  def "should retrieve the voter choices for multiple operation reference files"() {
    given:
    def ech0157Delivery = DefaultVoterChoiceConverterTest.classLoader.getResourceAsStream("eCH-0157-proportional-election.xml")
    def ech0159Delivery = DefaultVoterChoiceConverterTest.classLoader.getResourceAsStream("eCH-0159-variant-ballot.xml")
    def electionDefinitions = createElectionDefinitionsForECH0157("eCH-0157-proportional-election.xml")
    electionDefinitions.addAll(createElectionDefinitionsForECH0159("eCH-0159-variant-ballot.xml"))
    def parser = new DefaultVoterChoiceConverter()

    when:
    def voterChoices = parser.convertToVoterChoiceList(
            createElectionSet(electionDefinitions),
            [ech0159Delivery, ech0157Delivery] as InputStream[]
    )

    then: "choice 0 is candidate 20003 for election 1.CANDIDATES"
    def electionChoice0 = voterChoices.get(0) as VoterElectionChoice
    electionChoice0.getElection().getIdentifier() == "1.CANDIDATES"
    electionChoice0.getVoteId() == "1"
    electionChoice0.getCandidateId() == "20003"
    electionChoice0.getElectoralRollId() == "101"
    electionChoice0.isProportionalElection()
    !electionChoice0.isMajorityElection()
    !electionChoice0.isElectoralRoll()
    !electionChoice0.isBlankBallot()
    !electionChoice0.isEmptyCandidate()
    !electionChoice0.isEmptyList()

    then: "choice 41 is list 101 for election 1.LISTS"
    def electionChoice41 = voterChoices.get(41) as VoterElectionChoice
    electionChoice41.getElection().getIdentifier() == "1.LISTS"
    electionChoice41.getVoteId() == "1"
    electionChoice41.getCandidateId() == "101"
    electionChoice41.getElectoralRollId() == null
    electionChoice41.isProportionalElection()
    !electionChoice41.isMajorityElection()
    electionChoice41.isElectoralRoll()
    !electionChoice41.isBlankBallot()
    !electionChoice41.isEmptyCandidate()
    !electionChoice41.isEmptyList()

    then: "choice 43 is the empty list for election 1.LISTS"
    def electionChoice43 = voterChoices.get(43) as VoterElectionChoice
    electionChoice43.getElection().getIdentifier() == "1.LISTS"
    electionChoice43.getVoteId() == "1"
    electionChoice43.getCandidateId() == "EMPTY_LIST"
    electionChoice43.getElectoralRollId() == null
    electionChoice43.isProportionalElection()
    !electionChoice43.isMajorityElection()
    electionChoice43.isElectoralRoll()
    !electionChoice43.isBlankBallot()
    !electionChoice43.isEmptyCandidate()
    electionChoice43.isEmptyList()

    then: "choice 44 is YES for question CAN_GE_Q1.a"
    def votationChoice44 = voterChoices.get(44) as VoterVotationChoice
    votationChoice44.getElection().getIdentifier() == "CAN_GE_Q1.a"
    votationChoice44.getBallotId() == "CAN_GE_Q1"
    votationChoice44.getVoteId() == "201X04VE-CAN-GE"
    votationChoice44.getCandidateId() == "YES"
    !votationChoice44.isStandardBallot()
    votationChoice44.isVariantBallot()

    then: "choice 47 is NO for question CAN_GE_Q1.b"
    def votationChoice47 = voterChoices.get(47) as VoterVotationChoice
    votationChoice47.getElection().getIdentifier() == "CAN_GE_Q1.b"
    votationChoice47.getBallotId() == "CAN_GE_Q1"
    votationChoice47.getVoteId() == "201X04VE-CAN-GE"
    votationChoice47.getCandidateId() == "YES"
    !votationChoice47.isStandardBallot()
    votationChoice47.isVariantBallot()
  }

  def createElectionDefinitionsForECH0157(String electionDeliveryFileName) {
    def electionDeliveryStream = DefaultVoterChoiceConverterTest.classLoader.getResourceAsStream(electionDeliveryFileName)
    return new DefaultElectionConverter().convertToElectionDefinitionList(electionDeliveryStream)
  }

  def createElectionDefinitionsForECH0159(String votationDeliveryFilename) {
    def votationDeliveryStream = DefaultVoterChoiceConverterTest.classLoader.getResourceAsStream(votationDeliveryFilename)
    return new DefaultVotationConverter().convertToElectionDefinitionList(votationDeliveryStream)
  }

  def createElectionSet(List<ElectionDefinition> electionDefinitions) {
    def printingAuthority = new PrintingAuthorityWithPublicKey("pa1", BigInteger.ONE)
    return new ElectionSetBuilder().addElectionDefinitions(electionDefinitions)
            .addPrintingAuthorities([printingAuthority])
            .setCountingCircleCount(1)
            .setVoterCount(11)
            .build()
  }
}